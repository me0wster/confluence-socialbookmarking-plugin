package com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.AbstractLabelableEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.SaveContext;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.core.util.collection.EasyList;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultUser;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import junit.framework.TestCase;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BookmarkUtilsTest extends TestCase
{
    @Mock
	private PageManager pageManager;

    @Mock
	private ContentPropertyManager contentPropertyManager;

    @Mock
	private PermissionManager permissionManager;

    @Mock
	private LabelManager labelManager;

    @Mock
	private SpaceManager spaceManager;

    @Mock
	private I18NBeanFactory i18NBeanFactory;

    @Mock
    private I18NBean i18NBean;

    @Mock
	private LocaleManager localeManager;

    @Mock
	private SettingsManager settingsManager;

    @Mock
    private UserAccessor userAccessor;

    @Mock
    private VelocityHelperService velocityHelperService;

    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private Draft draft;

    private DefaultBookmarkUtils bookmarkUtils;

    private ConfluenceUserImpl user;

    private Settings globalSettings;

    public void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        
        when(localeManager.getLocale(Matchers.<User>anyObject())).thenReturn(Locale.getDefault());
        when(i18NBeanFactory.getI18NBean(Matchers.<Locale>anyObject())).thenReturn(i18NBean);

        globalSettings = new Settings();
        globalSettings.setDefaultEncoding("UTF-8");
        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        bookmarkUtils = new DefaultBookmarkUtils(pageManager, contentPropertyManager, permissionManager, labelManager, spaceManager, i18NBeanFactory, localeManager, new DefaultBookmarkMacroParser(xhtmlContent), settingsManager, userAccessor, velocityHelperService, xhtmlContent);

        user = new ConfluenceUserImpl(new DefaultUser("dchui"));
    }

    @Override
    protected void tearDown() throws Exception
    {
        pageManager = null;
        contentPropertyManager = null;
        permissionManager = null;
        labelManager = null;
        spaceManager = null;
        i18NBeanFactory = null;
        i18NBean = null;
        localeManager = null;
        settingsManager = null;
        userAccessor = null;
        velocityHelperService = null;
        xhtmlContent = null;
        draft = null;
        super.tearDown();
    }

    public void testGetBookmarkWhenPageIdIsNotValid()
    {
        assertNull(bookmarkUtils.getBookmark(0));
    }

    public void testGetBookmarkWhenUserHasNoPermissionToViewPage()
    {
        try
        {
            final long pageId = 1;
            final Page page = new Page();

            page.setId(pageId);
            AuthenticatedUserThreadLocal.setUser(user);

            when(pageManager.getPage(pageId)).thenReturn(page);

            assertNull(bookmarkUtils.getBookmark(pageId));


        }
        finally
        {
            AuthenticatedUserThreadLocal.setUser(null);
        }
    }

    public void testGetBookmarkWhenPageIsNotABookmark()
    {
        try
        {
            final long pageId = 1;
            final Page page = new Page();

            page.setId(pageId);

            AuthenticatedUserThreadLocal.set(user);

            when(pageManager.getPage(pageId)).thenReturn(page);
            when(permissionManager.hasPermission(user, Permission.VIEW, page)).thenReturn(true);

            assertNull(bookmarkUtils.getBookmark(pageId));


        }
        finally
        {
            AuthenticatedUserThreadLocal.set(null);
        }
    }

    /**
     * Simple mock page class to allow easy setting of labels into a page
     */
    private class MockPage extends Page
    {

        private List<Label> labels;

        public void setLabels(List<Label> labels)
        {
            this.labels = labels;
        }

        @Override
        public List<Label> getLabels()
        {
            return labels;
        }
    }

    public void testGetBookmarkWhenPageIsABookmarkWithNonAnonymousUser()
    {
        try
        {
            final long pageId = 1;
            final Page page = new MockPage();
            final String bookmarkUrl = "http://www.atlassian.com/";
            final Bookmark bookmark;

            page.setId(pageId);
            page.setCreationDate(new Date());
            page.setSpace(new Space("TST"));
            page.setComments(Collections.EMPTY_LIST);
            page.setCreator(new ConfluenceUserImpl(new DefaultUser("David Chui")));

            Label label1 = new Label("label1");
            Label label2 = new Label("label2");
            ((MockPage) page).setLabels(Arrays.asList(label1, label2));

            AuthenticatedUserThreadLocal.setUser(user);

            when(pageManager.getById(pageId)).thenReturn(page);
            when(permissionManager.hasPermission(eq(user), (Permission) anyObject(), eq(page))).thenReturn(true);
            when(contentPropertyManager.getStringProperty(page, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY)).thenReturn("true");
            when(contentPropertyManager.getTextProperty(page, BookmarkUtils.URL_PROPERTY_KEY)).thenReturn(bookmarkUrl);
            when(userAccessor.getUser(page.getCreatorName())).thenReturn(user);

            bookmark = bookmarkUtils.getBookmark(pageId);
            assertNotNull(bookmark);
            assertEquals(bookmarkUrl, bookmark.getUrl());

            // Check that the returned labels match the labels on the input page
            List bookmarkLabels = bookmark.getLabels();
            assertEquals(2, bookmarkLabels.size());
            assertTrue(bookmarkLabels.contains(label1));
            assertTrue(bookmarkLabels.contains(label2));

        }
        finally
        {
            AuthenticatedUserThreadLocal.setUser(null);
        }
    }


    public void testGetBookmarkWhenPageIsABookmarkWithAnonymousUser()
    {
        try
        {
            final long pageId = 1;
            final Page page = new MockPage();
            final String bookmarkUrl = "http://www.atlassian.com/";
            final Bookmark bookmark;

            page.setId(pageId);
            page.setCreationDate(new Date());
            page.setSpace(new Space("TST"));
            page.setComments(Collections.EMPTY_LIST);
            page.setCreator(new ConfluenceUserImpl(new DefaultUser("David Chui")));

            Label label1 = new Label("label1");
            Label label2 = new Label("label2");
            ((MockPage) page).setLabels(Arrays.asList(label1, label2));


            AuthenticatedUserThreadLocal.setUser(null);

            when(pageManager.getById(pageId)).thenReturn(page);
            when(permissionManager.hasPermission(eq((User) null), eq(Permission.VIEW), eq(page))).thenReturn(true);
            when(contentPropertyManager.getStringProperty(page, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY)).thenReturn("true");
            when(contentPropertyManager.getTextProperty(page, BookmarkUtils.URL_PROPERTY_KEY)).thenReturn(bookmarkUrl);
            when(userAccessor.getUser(page.getCreatorName())).thenReturn(user);

            bookmark = bookmarkUtils.getBookmark(pageId);
            assertNotNull(bookmark);
            assertEquals(bookmarkUrl, bookmark.getUrl());
            /* Anonymous user will never get edit and remove perms */
            assertFalse(bookmark.isEditPermission());
            assertFalse(bookmark.isRemovePermission());


            // Check that the returned labels match the labels on the input page
            List bookmarkLabels = bookmark.getLabels();
            assertEquals(2, bookmarkLabels.size());
            assertTrue(bookmarkLabels.contains(label1));
            assertTrue(bookmarkLabels.contains(label2));

        }
        finally
        {
            AuthenticatedUserThreadLocal.setUser(null);
        }
    }

    public void testGetBookmarkWhenPageHasPrivateLabels()
    {

        try
        {
            final long pageId = 1;
            final Page page = new MockPage();
            final String bookmarkUrl = "http://www.atlassian.com/";
            final Bookmark bookmark;

            page.setId(pageId);
            page.setCreationDate(new Date());
            page.setSpace(new Space("TST"));
            page.setComments(Collections.EMPTY_LIST);
            page.setCreator(new ConfluenceUserImpl(new DefaultUser("David Chui")));

            Label label1 = new Label("label1");
            Label label2 = new Label("label2");
            Label privateLabel = new Label("privateLabel", Namespace.PERSONAL, user);
            Label differentUserPrivateLabel = new Label("differentUserPrivateLabel", Namespace.PERSONAL, new ConfluenceUserImpl(new DefaultUser("anthoerUser")));
            ((MockPage) page).setLabels(Arrays.asList(label1, differentUserPrivateLabel, privateLabel, label2));

            AuthenticatedUserThreadLocal.set(user);

            when(pageManager.getById(pageId)).thenReturn(page);
            when(permissionManager.hasPermission(eq(user), (Permission) anyObject(), eq(page))).thenReturn(true);
            when(contentPropertyManager.getStringProperty(page, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY)).thenReturn("true");
            when(contentPropertyManager.getTextProperty(page, BookmarkUtils.URL_PROPERTY_KEY)).thenReturn(bookmarkUrl);
            when(userAccessor.getUser(page.getCreatorName())).thenReturn(user);

            bookmark = bookmarkUtils.getBookmark(pageId);
            assertNotNull(bookmark);
            assertEquals(bookmarkUrl, bookmark.getUrl());

            // Check that the returned labels match the labels on the input
            // page, the private label should not be displayed for other users.
            List bookmarkLabels = bookmark.getLabels();
            assertEquals(3, bookmarkLabels.size());
            assertTrue(bookmarkLabels.containsAll(EasyList.build(label1, label2, privateLabel)));

        }
        finally
        {
            AuthenticatedUserThreadLocal.setUser(null);
        }

    }

    public void testGetBookmarkParentWhenBookmarkParentPageDoesNotExistAndCreationIsRequested()
    {
        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(new HashMap<String, Object>());

        final Space space = new Space("TST");
        final Page bookmarksParentPage;

        bookmarksParentPage = bookmarkUtils.getBookmarkParent(space, true);
        assertNotNull(bookmarksParentPage);
        assertEquals(BookmarkUtils.BOOKMARK_PARENT_PAGE, bookmarksParentPage.getTitle());

        verify(pageManager).saveContentEntity(
                argThat(new ArgumentMatcher<ContentEntityObject>()
                {
                    public boolean matches(Object o)
                    {
                        return o instanceof Page && ((Page) o).getTitle().equals(BookmarkUtils.BOOKMARK_PARENT_PAGE);
                    }
                }), (SaveContext) anyObject()
        );
    }

    public void testGetBookmarkParentWhenBookmarkParentPageDoesNotExistAndCreationIsNotRequested()
    {

        assertNull(bookmarkUtils.getBookmarkParent(new Space("TST"), false));
    }

    public void testGetBookmarkParentWhenBookmarkParentPageExists()
    {
        final Space space = new Space("TST");
        final Page bookmarksParentPage = new Page();

        when(pageManager.getPage(space.getKey(), BookmarkUtils.BOOKMARK_PARENT_PAGE)).thenReturn(bookmarksParentPage);

        assertSame(bookmarksParentPage, bookmarkUtils.getBookmarkParent(space, true));
    }

    public void testViewableBookmarksWithPermissionManagerDenyingViewOnAllOfThePotentialViewableOnes()
    {
        final Set<Page> potentiallyViewableBookmarks = new LinkedHashSet<Page>();

        /* Create some pages */
        for (int i = 1; i <= 2; ++i)
        {
            final Page page = new Page();
            page.setId(i);

            potentiallyViewableBookmarks.add(page);
        }

        assertEquals(0, bookmarkUtils.getViewableBookmarks(potentiallyViewableBookmarks, user).size());

        verify(permissionManager, times(2)).hasPermission(eq(user), eq(Permission.VIEW), isA(Page.class));
    }

    public void testViewableBookmarksWithPermissionManagerDenyingViewOnSomeOfThePotentialViewableOnes()
    {
        final Set<Page> potentiallyViewableBookmarks = new LinkedHashSet<Page>();
        final Page page1 = new Page();
        final Page page2 = new Page();
        final String bookmarkUrl = "http://www.atlassian.com/";
        final List viewableBookmarks;
        final Bookmark viewableBookmark;

        page1.setId(1);
        potentiallyViewableBookmarks.add(page1);

        page2.setId(2);
        page2.setCreationDate(new Date());
        page2.setSpace(new Space("TST"));
        page2.setCreator(new ConfluenceUserImpl(new DefaultUser("David Chui")));
        potentiallyViewableBookmarks.add(page2);

        when(userAccessor.getUser(page2.getCreatorName())).thenReturn(user);
        when(permissionManager.hasPermission(eq(user), eq(Permission.VIEW), isA(Page.class)))
                .thenReturn(false)
                .thenReturn(true);

        when(permissionManager.hasPermission(eq(user), eq(Permission.EDIT), same(page2))).thenReturn(true);
        when(permissionManager.hasPermission(eq(user), eq(Permission.REMOVE), same(page2))).thenReturn(true);

        when(contentPropertyManager.getStringProperty(page2, BookmarkUtils.ISBOOKMARK_PROPERTY_KEY)).thenReturn("true");
        when(contentPropertyManager.getTextProperty(page2, BookmarkUtils.URL_PROPERTY_KEY)).thenReturn(bookmarkUrl);

        viewableBookmarks = bookmarkUtils.getViewableBookmarks(potentiallyViewableBookmarks, user);
        assertEquals(1, viewableBookmarks.size());

        viewableBookmark = (Bookmark) viewableBookmarks.get(0);

        assertEquals(page2.getIdAsString(), viewableBookmark.getId());
        assertEquals(bookmarkUrl, viewableBookmark.getUrl());
    }

    public void testGetBookmarkListWhenSpacesDoesNotHaveBookmarkParentPage()
    {
        when(spaceManager.getAllSpaces()).thenReturn(Arrays.asList(new Space("TST")));

        assertEquals(Collections.EMPTY_LIST, bookmarkUtils.getBookmarkList(
                new HashSet<String>(Arrays.asList(BookmarkUtils.SPACES_ALL)),
                StringUtils.EMPTY,
                StringUtils.EMPTY,
                null,
                Integer.MAX_VALUE,
                1,
                false).getBookmarkList());
    }

    public void testGetBookmarkListFilteredByCreatorsCaseInSensitive()
    {
        final Space space = new Space("TST");
        final Page spaceBookmarkPage1 = new Page();
        final Page spaceBookmarkPage2 = new Page();
        final Page spaceBookmarkParentPage = new Page()
        {
            public List<Page> getChildren()
            {
                /* Always return two pages as bookmarks */
                return Arrays.asList(spaceBookmarkPage1, spaceBookmarkPage2);
            }
        };
        
        final String creator = "John.Doe";
        spaceBookmarkPage1.setCreator(new ConfluenceUserImpl(new DefaultUser(creator)));

        for (Page aPage : Arrays.asList(spaceBookmarkPage1, spaceBookmarkPage2, spaceBookmarkParentPage))
        {
            aPage.setId(Long.parseLong(RandomStringUtils.randomNumeric(5)));
            aPage.setSpace(space);
            aPage.setCreationDate(new Date());
        }

        when(spaceManager.getAllSpaces()).thenReturn(Arrays.asList(space));
        when(pageManager.getPage(space.getKey(), BookmarkUtils.BOOKMARK_PARENT_PAGE)).thenReturn(spaceBookmarkParentPage);
        when(permissionManager.hasPermission(Matchers.<User>anyObject(), eq(Permission.VIEW), Matchers.<Object>anyObject())).thenReturn(true);
        when(contentPropertyManager.getStringProperty(Matchers.<ContentEntityObject>anyObject(), eq(BookmarkUtils.ISBOOKMARK_PROPERTY_KEY))).thenReturn(Boolean.TRUE.toString());

        final List<Bookmark> bookmarkList = bookmarkUtils.getBookmarkList(
                new HashSet<String>(Arrays.asList(BookmarkUtils.SPACES_ALL)),
                null,
                creator,
                null,
                Integer.MAX_VALUE,
                1,
                false).getBookmarkList();

        assertEquals(1, bookmarkList.size());
        assertEquals(spaceBookmarkPage1.getIdAsString(), bookmarkList.get(0).getId());
    }

    public void testGetBookmarkListFilteredByLabels()
    {
        final Space space = new Space("TST");
        final Page spaceBookmarkPage1 = new Page();
        final Page spaceBookmarkPage2 = new Page();
        final Page spaceBookmarkParentPage = new Page()
        {
            public List<Page> getChildren()
            {
                /* Always return two pages as bookmarks */
                return Arrays.asList(spaceBookmarkPage1, spaceBookmarkPage2);
            }
        };

        for (Page aPage : Arrays.asList(spaceBookmarkPage1, spaceBookmarkPage2, spaceBookmarkParentPage))
        {
            aPage.setId(Long.parseLong(RandomStringUtils.randomNumeric(5)));
            aPage.setSpace(space);
            aPage.setCreationDate(new Date());
        }



        when(spaceManager.getAllSpaces()).thenReturn(Arrays.asList(space));
        when(pageManager.getPage(space.getKey(), BookmarkUtils.BOOKMARK_PARENT_PAGE)).thenReturn(spaceBookmarkParentPage);
        when(permissionManager.hasPermission(Matchers.<User>anyObject(), eq(Permission.VIEW), Matchers.<Object>anyObject())).thenReturn(true);
        when(contentPropertyManager.getStringProperty(Matchers.<ContentEntityObject>anyObject(), eq(BookmarkUtils.ISBOOKMARK_PROPERTY_KEY))).thenReturn(Boolean.TRUE.toString());

        final Label labelToFilter = new Label("label1");
        when(labelManager.getLabel(labelToFilter.getName())).thenReturn(labelToFilter);

        when(labelManager.getCurrentContentForLabelAndSpace(labelToFilter, space.getKey())).thenAnswer(
                new Answer<List<AbstractLabelableEntityObject>>()
                {
                    @Override
                    public List<AbstractLabelableEntityObject> answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return Arrays.asList((AbstractLabelableEntityObject) spaceBookmarkPage1);
                    }
                }
        );


        final List<Bookmark> bookmarkList = bookmarkUtils.getBookmarkList(
                new HashSet<String>(Arrays.asList(BookmarkUtils.SPACES_ALL)),
                labelToFilter.getName(),
                StringUtils.EMPTY,
                null,
                Integer.MAX_VALUE,
                1,
                false).getBookmarkList();

        assertEquals(1, bookmarkList.size());
        assertEquals(spaceBookmarkPage1.getIdAsString(), bookmarkList.get(0).getId());
    }

    public void testCreateRssUrl()
    {
        Space anchorSpace = new Space("TST");

        /* "spaceKeys" should be in the URL if the spaceKey parameter is not blank and null  */
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, "TP, TP01, TP02", null, null, 0, false, null).indexOf("spaceKeys") >= 0);

        /* "spaceKeys" should not be in the URL if the spaceKey parameter is blank or null */
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, "  ", null, null, 0, false, null).indexOf("spaceKeys") == -1);
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, null).indexOf("spaceKeys") == -1);


        /* "labels" should be in the URL if the labels parameter is not blank and null  */
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, "label1, label2, label3", null, 0, false, null).indexOf("labels") >= 0);

        /* "labels" should not be in the URL if the labels parameter is blank or null */
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, "  ", null, 0, false, null).indexOf("labels") == -1);
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, null).indexOf("labels") == -1);


        /* "sort" should be in the URL if the sort parameter is not blank and null  */
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, "sort1, sort2, sort3", 0, false, null).indexOf("sort") >= 0);

        /* "sort" should not be in the URL if the sort parameter is blank or null */
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, "  ", null, 0, false, null).indexOf("sort") == -1);
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, null).indexOf("sort") == -1);

        /* "reverse" parameter should be in the URL if the reverse parameter is true */
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, true, null).indexOf("reverse") >= 0);
        /* "reverse" parameter should not be in the URL if the reverse parameter is false */
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, null).indexOf("reverse") == -1);

        /* Display parameters should appear in the URL if their values are false */
        final Map<String, Object> displayParams = new HashMap<String, Object>();
        displayParams.put("display1", Boolean.FALSE);
        displayParams.put("display2", Boolean.FALSE);
        displayParams.put("display3", Boolean.FALSE);
        displayParams.put("display4", Boolean.FALSE);

        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, displayParams).indexOf("display1") >= 0);
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, displayParams).indexOf("display2") >= 0);
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, displayParams).indexOf("display3") >= 0);
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, displayParams).indexOf("display4") >= 0);


        displayParams.put("display1", Boolean.TRUE);
        displayParams.put("display2", Boolean.TRUE);
        displayParams.put("display3", Boolean.TRUE);
        displayParams.put("display4", Boolean.TRUE);

        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, displayParams).indexOf("display1") == -1);
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, displayParams).indexOf("display2") == -1);
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, displayParams).indexOf("display3") == -1);
        assertTrue(bookmarkUtils.createRssUrl(anchorSpace, null, null, null, 0, false, displayParams).indexOf("display4") == -1);
    }

    public void testCreateSyndFeed()
    {
        final List<Bookmark> bookmarks = new ArrayList<Bookmark>();
        final Label commonBookmarkLabel = new Label("label1");
        final SyndFeed syndFeed;
        final List feedEntries;

        /* Create a few bookmarks */
        for (int i = 1; i <= 4; ++i)
        {
            final Bookmark bookmark = new Bookmark();

            bookmark.setTitle("Bookmark-" + 1);
            bookmark.setUrl("http://localhost/viewookmark.action?bookmark=" + i); /* LOL? */
            bookmark.setCreator(user);
            bookmark.setAddedDate(new Date());
            bookmark.setLabels(Arrays.asList(commonBookmarkLabel));

            bookmarks.add(bookmark);
        }

        when(i18NBean.getText(anyString(), isA(List.class))).thenReturn("");

        globalSettings.setBaseUrl("BaseURL");

        syndFeed = bookmarkUtils.createSyndFeed(bookmarks, null, null);
        feedEntries = syndFeed.getEntries();
        assertEquals(bookmarks.size(), feedEntries.size());

        for (int i = 0; i < bookmarks.size(); ++i)
        {
            final Bookmark bookmark = bookmarks.get(i);
            final SyndEntry entry = (SyndEntry) feedEntries.get(i);

            assertEquals(bookmark.getTitle(), entry.getTitle());
            assertEquals(bookmark.getUrl(), entry.getLink());
            assertEquals(bookmark.getAddedDate(), entry.getPublishedDate());
        }
    }


    /**
     * Testing CreateComparator method.
     * <p/>
     * Valid comparator types are currently date, author and title
     */
    public void testCreateComparator()
    {

        // Test passing single sort param
        Comparator result = bookmarkUtils.createComparator(BookmarkUtils.SORT_CREATION);
        AbstractBookmarkComparator expected = new BookmarkDateComparator();
        checkComparatorMatches("sort by date only", result, expected);

        result = bookmarkUtils.createComparator(BookmarkUtils.SORT_CREATOR);
        expected = new BookmarkAuthorComparator();
        checkComparatorMatches("sort by author only", result, expected);

        result = bookmarkUtils.createComparator(BookmarkUtils.SORT_TITLE);
        expected = new BookmarkTitleComparator();
        checkComparatorMatches("sort by title only", result, expected);

        // Test passing multiple sort params
        result = bookmarkUtils.createComparator(BookmarkUtils.SORT_CREATOR + ", " + BookmarkUtils.SORT_CREATION);
        expected = new BookmarkAuthorComparator();
        expected.setNextComparator(new BookmarkDateComparator());
        checkComparatorMatches("sort by author then date", result, expected);

        result = bookmarkUtils.createComparator(BookmarkUtils.SORT_CREATION + " " + BookmarkUtils.SORT_TITLE + "," + BookmarkUtils.SORT_CREATOR);
        expected = new BookmarkDateComparator();
        AbstractBookmarkComparator expected2 = new BookmarkTitleComparator();
        AbstractBookmarkComparator expected3 = new BookmarkAuthorComparator();
        expected2.setNextComparator(expected3);
        expected.setNextComparator(expected2);
        checkComparatorMatches("sort by date then title then author", result, expected);

        // null, empty, invalid sort params - should return null comparator
        result = bookmarkUtils.createComparator(null);
        assertNull("null input should return null result", result);

        result = bookmarkUtils.createComparator("");
        assertNull("empty input should return null result", result);

        result = bookmarkUtils.createComparator("invalidsortString");
        assertNull("invalid input should return null result", result);

        result = bookmarkUtils.createComparator(BookmarkUtils.SORT_CREATOR + "invalid bit at the end");
        assertNull("invalid inuput should return null result", result);

        // valid sort string followed by invalid string should return all
        // comparators that are valid up to the invalid token.
        result = bookmarkUtils.createComparator(BookmarkUtils.SORT_TITLE + ", invalidToke");
        expected = new BookmarkTitleComparator();
        checkComparatorMatches("sort by title only + invalid token", result, expected);

        result = bookmarkUtils.createComparator(BookmarkUtils.SORT_CREATOR + ", " + BookmarkUtils.SORT_CREATION + ", invalidToken, " + BookmarkUtils.SORT_TITLE);
        expected = new BookmarkAuthorComparator();
        expected.setNextComparator(new BookmarkDateComparator());
        checkComparatorMatches("sort by author then date + invalid token", result, expected);


    }

    /**
     * Helper method for checking that a created AbstractBookmarkComparator
     * matches the given example.
     *
     * @param testCase       String describing the test case that will be printed when the
     *                       test fails.
     * @param test
     * @param expectedResult
     */
    protected void checkComparatorMatches(String testCase, Comparator test, AbstractBookmarkComparator expectedResult)
    {

        if (expectedResult == null)
        {
            assertNull("expected result is null " + testCase, test);
        }
        else
        {

            // Check the testing comparator is not null
            assertNotNull("test comparator should not be null, " + testCase, test);

            // Check that the test comparator matches the type of the expectedResult
            assertEquals("Expected Comparator class doesn't match result, " + testCase, expectedResult.getClass(), test.getClass());

            AbstractBookmarkComparator testResult = (AbstractBookmarkComparator) test;

            // Check that the sub comparators match if present
            if (expectedResult.getNextComparator() != null)
            {
                assertNotNull("Next comparator should be null, " + testCase, testResult.getNextComparator());

                checkComparatorMatches(testCase, testResult.getNextComparator(), expectedResult.getNextComparator());

            }
        }
    }

    /**
     * Very simple test to check getBookmarkParent handles null space param
     */
    public void testGetBookmarkParent()
    {

        Page parentPage = bookmarkUtils.getBookmarkParent(null, false);

        assertNull("parentPage shold be null for null space and no create", parentPage);

        parentPage = bookmarkUtils.getBookmarkParent(null, true);

        assertNull("parentPage should be null for null space and create", parentPage);

    }
    
    public void testConvertDraftToBookmark() throws XMLStreamException, XhtmlException
    {
    	final String bookmarkUrl = "http://www.atlassian.com/";
    	final long pageId = 1;
    	final String spaceKey = "TST";
    	Bookmark bookmark = newBookmark();
    	
        when(contentPropertyManager.getTextProperty(draft, BookmarkUtils.URL_PROPERTY_KEY)).thenReturn(bookmarkUrl);
        AuthenticatedUserThreadLocal.setUser(user);
        Space space = new Space(spaceKey);
        space.setName("Test Space");
        draft.setId(pageId);
        
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(draft.getCreatorName()).thenReturn("dchui");
        when(userAccessor.getUser(draft.getCreatorName())).thenReturn(user);
        when(draft.getLabels()).thenReturn(Arrays.asList(new Label("label1"), new Label("label2")));
        when(draft.getType()).thenReturn(Draft.CONTENT_TYPE);
        when(draft.getCreationDate()).thenReturn(new Date());
        when(draft.getDraftSpaceKey()).thenReturn(spaceKey);
        when(draft.getBodyAsString()).thenReturn("{bookmark:url=http://www.atlassian.com/}{bookmark}");
        when(draft.getUserAccessor()).thenReturn(userAccessor);
        when(draft.getTitle()).thenReturn("Atlassian");
        when(draft.getIdAsString()).thenReturn(String.valueOf(pageId));
        when(draft.getVisibleLabels(user)).thenReturn(Arrays.asList(new Label("label1"), new Label("label2")));

        when(xhtmlContent.convertStorageToView(Matchers.anyString(), Matchers.<ConversionContext>anyObject())).thenReturn("");
    	
    	Bookmark convertedBookmark = bookmarkUtils.convertPageToBookmark(draft, user);
    	assertEquals(bookmark.getTitle(), convertedBookmark.getTitle());
    	assertEquals(bookmark.getUrl(), convertedBookmark.getUrl());
    	assertEquals(bookmark.getId(), convertedBookmark.getId());
    	assertEquals(bookmark.getSpaceName(), convertedBookmark.getSpaceName());
    	assertEquals(bookmark.getSpaceUrlPath(), convertedBookmark.getSpaceUrlPath());
    	assertEquals(bookmark.getDescription(), convertedBookmark.getDescription());
    	assertEquals(bookmark.getCreator(), convertedBookmark.getCreator());
    	assertEquals(bookmark.getLabels(), convertedBookmark.getLabels());
    }

    private Bookmark newBookmark()
    {
    	final long pageId = 1;
    	final String spaceKey = "TST";
    	Bookmark bookmark = new Bookmark();
    	bookmark.setTitle("Atlassian");
    	bookmark.setUrl("http://www.atlassian.com/");
    	bookmark.setAddedDate(new Date());
    	Label label1 = new Label("label1");
    	Label label2 = new Label("label2");
    	bookmark.setLabels(Arrays.asList(label1, label2));
    	bookmark.setId(String.valueOf(pageId));
    	Space space = new Space(spaceKey);
    	bookmark.setSpaceName("Test Space");
    	bookmark.setSpaceUrlPath(space.getUrlPath());
    	bookmark.setDescription("");

        DefaultUser backingUser = (DefaultUser) user.getBackingUser();
        backingUser.setFullName("David Chui");
        bookmark.setCreator(user);
    	
    	return bookmark;
    }

    public void testGetBookmarkListWithEmptyLabelsStringParam()
    {
        String spaceKey = "tst";
        Page bookmarksPage = new Page();

        when(spaceManager.getSpace(spaceKey)).thenReturn(new Space(spaceKey));
        when(pageManager.getPage(spaceKey, ".bookmarks")).thenReturn(bookmarksPage);

        bookmarkUtils.getBookmarkList(
                new HashSet<String>(Arrays.asList(spaceKey)),
                ", ",
                null,
                null,
                Short.MAX_VALUE,
                0
        );

        verify(labelManager, never()).getLabel(anyString());
    }

    public void testGetBookmarkListWithCommaSeparatedLabelsStringParam()
    {
        String spaceKey = "tst";
        Page bookmarksPage = new Page();

        when(spaceManager.getSpace(spaceKey)).thenReturn(new Space(spaceKey));
        when(pageManager.getPage(spaceKey, ".bookmarks")).thenReturn(bookmarksPage);

        bookmarkUtils.getBookmarkList(
                new HashSet<String>(Arrays.asList(spaceKey)),
                "label1,label2",
                null,
                null,
                Short.MAX_VALUE,
                0
        );

        verify(labelManager).getLabel("label1");
        verify(labelManager).getLabel("label2");
    }

    public void testGetBookmarkListCreatorsParamCommaAndSpaceSeparated()
    {
        String spaceKey = "tst";
        Page bookmarksPage = new Page();
        Page bookmark1 = new Page();

        bookmarksPage.setChildren(Arrays.asList(bookmark1));

        Space space = new Space(spaceKey);

        bookmark1.setSpace(space);
        bookmark1.setParentPage(bookmarksPage);

        String bookmarkTitle = "foobar";
        bookmark1.setTitle(bookmarkTitle);

        bookmark1.setCreationDate(new Date());

        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(pageManager.getPage(spaceKey, ".bookmarks")).thenReturn(bookmarksPage);
        when(permissionManager.hasPermission(Matchers.<User>anyObject(), eq(Permission.VIEW), Matchers.<Object>anyObject())).thenReturn(true);
        when(contentPropertyManager.getStringProperty(Matchers.<ContentEntityObject>anyObject(), eq(BookmarkUtils.ISBOOKMARK_PROPERTY_KEY))).thenReturn(Boolean.TRUE.toString());

        Bookmarks bookmarks = bookmarkUtils.getBookmarkList(
                new HashSet<String>(Arrays.asList(spaceKey)),
                null,
                ", ",
                null,
                Short.MAX_VALUE,
                0
        );

        assertEquals(1, bookmarks.getBookmarkList().size());
        assertEquals(bookmarkTitle, bookmarks.getBookmarkList().iterator().next().getTitle());
    }

    public void testGetBookmarkListFilteredByCreator()
    {
        String spaceKey = "tst";
        Page bookmarksPage = new Page();
        Page bookmark1 = new Page();
        Page bookmark2 = new Page();

        bookmarksPage.setChildren(Arrays.asList(bookmark1, bookmark2));

        Space space = new Space(spaceKey);
        String bookmarkTitleBase = "foobar-";
        int bookmarkTitleCounter = 0;

        for (Page bookmarkPage : Arrays.asList(bookmark1, bookmark2))
        {
            bookmarkPage.setSpace(space);
            bookmarkPage.setParentPage(bookmarksPage);
            bookmarkPage.setCreationDate(new Date());
            bookmarkPage.setTitle(bookmarkTitleBase + bookmarkTitleCounter++);
        }

        bookmark2.setCreator(new ConfluenceUserImpl(new DefaultUser("jdoe")));

        when(spaceManager.getSpace(spaceKey)).thenReturn(space);
        when(pageManager.getPage(spaceKey, ".bookmarks")).thenReturn(bookmarksPage);
        when(permissionManager.hasPermission(Matchers.<User>anyObject(), eq(Permission.VIEW), Matchers.<Object>anyObject())).thenReturn(true);
        when(contentPropertyManager.getStringProperty(Matchers.<ContentEntityObject>anyObject(), eq(BookmarkUtils.ISBOOKMARK_PROPERTY_KEY))).thenReturn(Boolean.TRUE.toString());
        when(userAccessor.getUser(anyString())).thenAnswer(
                new Answer<Object>()
                {
                    public Object answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return new DefaultUser(invocationOnMock.getArguments()[0].toString());
                    }
                }
        );

        Bookmarks bookmarks = bookmarkUtils.getBookmarkList(
                new HashSet<String>(Arrays.asList(spaceKey)),
                null,
                "jdoe",
                null,
                Short.MAX_VALUE,
                0
        );

        assertEquals(1, bookmarks.getBookmarkList().size());
        assertEquals(bookmark2.getTitle(), bookmarks.getBookmarkList().iterator().next().getTitle());
    }

    public void testGetBookmarksWithNonExistentLabel()
    {
        String spaceKey = "tst";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        Page bookmarksPage = new Page();
        
        Page bookmark1 = new Page();
        bookmark1.setSpace(space);
        bookmark1.setParentPage(bookmarksPage);
        bookmark1.setCreationDate(new Date());
        bookmark1.setTitle("Test bookmark");

        bookmarksPage.setChildren(Arrays.asList(bookmark1));
        when(pageManager.getPage(spaceKey, ".bookmarks")).thenReturn(bookmarksPage);
        when(labelManager.getLabel("exists")).thenReturn(new Label("exists"));

        bookmarkUtils.getBookmarkList(
                new HashSet<String>(Arrays.asList(spaceKey)),
                "exists,doesnotexist",
                null, null, Integer.MAX_VALUE, 0
        );


        verify(labelManager, times(1)).getCurrentContentForLabelAndSpace(Matchers.argThat(
                new ArgumentMatcher<Label>()
                {
                    @Override
                    public boolean matches(Object o)
                    {

                        return StringUtils.equals(((Label) o).getName(), "exists");
                    }
                }
        ), Matchers.eq(spaceKey));
        verify(labelManager, never()).getCurrentContentForLabelAndSpace(Matchers.argThat(
                new ArgumentMatcher<Label>()
                {
                    @Override
                    public boolean matches(Object o)
                    {

                        return StringUtils.equals(((Label) o).getName(), "doesnotexist");
                    }
                }
        ), Matchers.eq(spaceKey));
    }
}
