package it.com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

public class BookmarksMacroTestCase extends AbstractConfluencePluginWebTestCase
{
    public void testXssMaxParameter()
    {
        final long pageId = createPage("testXssMaxParameter", "{bookmarks:max=x\"</pre><script>alert(1)</script>}");

        getIndexHelper().update();
        viewPageById(pageId);

        String macroTitle = getElementTextByXPath("//div[@class='wiki-content']//div[@class='blogSurtitle bookmarkListTitle']//h2");
        assertEquals("The most recent bookmarks in Demonstration Space", macroTitle);
    }

    private long createPage(String title, String content)
    {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle(title);
        pageHelper.setContent(content);
        pageHelper.create();
        return pageHelper.getId();
    }

    private void viewPageById(long entityId)
    {
        gotoPage("/pages/viewpage.action?pageId=" + entityId);
    }
}
