package it.com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.helper.MailServerHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import com.atlassian.confluence.util.GeneralUtil;
import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;
import com.sun.syndication.feed.synd.SyndCategoryImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.SAXException;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BookmarkTestCase extends AbstractConfluencePluginWebTestCase
{
	private static final long DEFAULT_WAIT = 1000L * 10;

	private String testSpaceKey;

	private String testSpaceName;

	private String baseUrl;

	private Properties dumbsterConfig;

	private void createTestSpace()
	{
		final SpaceHelper spaceHelper = getSpaceHelper();

		spaceHelper.setKey(testSpaceKey = "tst");
		spaceHelper.setName(testSpaceName = "Test Space");
		spaceHelper.setDescription("Test Space");

		assertTrue(spaceHelper.create());
	}

	protected void setUp() throws Exception
	{
		super.setUp();
		createTestSpace();
		setBaseUrl();
	}

	private void setBaseUrl()
	{
		try
		{
			gotoPageWithEscalatedPrivileges("/admin/editgeneralconfig.action");
			setWorkingForm("editgeneralconfig");
			baseUrl = getElementAttributByXPath("//form[@name='editgeneralconfig']//input[@name='domainName']", "value");
		}
		finally
		{
			dropEscalatedPrivileges();
		}
	}

	private Pattern getEditBookmarkPattern(String spaceName, String bookmarkTitle)
	{
		return Pattern.compile("id=\"socialbookmark-edit-" + spaceName + ":" + bookmarkTitle + "\".*href=\"(.+)\"", Pattern.MULTILINE);
	}

	private Pattern getRemoveBookmarkPattern(String spaceName, String bookmarkTitle)
	{
		return Pattern.compile("id=\"socialbookmark-remove-" + spaceName + ":" + bookmarkTitle + "\".*href=\"(.+)\"", Pattern.MULTILINE);
	}

	private Pattern getViewBookmarkPattern(String spaceName, String bookmarkTitle)
	{
		return Pattern.compile("id=\"socialbookmark-view-" + spaceName + ":" + bookmarkTitle + "\".*href=\"(.+)\"", Pattern.MULTILINE);
	}

	private void createBookmark(
			final String bookmarkTitle,
			final String bookmarkLocation,
			final String spaceKey,
			final String bookmarkDescription,
			final String labels) throws UnsupportedEncodingException
			{
		final SpaceHelper spaceHelper = getSpaceHelper(spaceKey);

		assertTrue(spaceHelper.read());

		gotoPage("/plugins/socialbookmarking/updatebookmark.action?spaceKey=" + URLEncoder.encode(spaceKey, "UTF-8") + "&redirect=spacebookmarks");

		/* Make sure labels field exists!! */
		assertFormElementPresent("labelsString");

		setWorkingForm("bookmarkForm");

		setTextField("title", bookmarkTitle);
		setTextField("url", bookmarkLocation);
		selectOption("spaceKey", spaceHelper.getName());
		setTextField("description", bookmarkDescription);

		if (StringUtils.isNotBlank(labels))
		{
			setTextField("labelsString", labels);
		}

		submit("save");

		assertTitleEquals("Space Bookmarks - " + spaceHelper.getName() + " - Confluence");
		assertLinkPresentWithText(bookmarkTitle);
    }

	private void createBookmarkInPersonalSpace(
			final String bookmarkTitle,
			final String bookmarkLocation,
			final UserHelper userHelper,
			final String bookmarkDescription,
			final String labels) throws UnsupportedEncodingException
			{

		gotoPage("/plugins/socialbookmarking/updatebookmark.action?spaceKey=" + URLEncoder.encode(userHelper.getName(), "UTF-8") + "&redirect=spacebookmarks");

		/* Make sure labels field exists!! */
		assertFormElementPresent("labelsString");

		setWorkingForm("bookmarkForm");

		setTextField("title", bookmarkTitle);
		setTextField("url", bookmarkLocation);
		selectOption("spaceKey", userHelper.getFullName());
		setTextField("description", bookmarkDescription);

		if (StringUtils.isNotBlank(labels))
		{
			setTextField("labelsString", labels);
		}

		submit("save");

		assertTitleEquals("Space Bookmarks - " + userHelper.getFullName() + " - Confluence");
			}

	private void removeBookmark(final String spaceKey, final String title)
	{
		final SpaceHelper spaceHelper = getSpaceHelper(spaceKey);

		assertTrue(spaceHelper.read());

		gotoPage("/spaces/viewspacesummary.action?key=" + spaceKey);
		clickLinkWithText("Bookmarks");
		clickLink("socialbookmark-remove-" + spaceHelper.getName() + ":" + title);
	}


	public void testBookmarkTabExists()
	{
		gotoPage("/spaces/viewspacesummary.action?key=ds");

		assertLinkPresentWithText("Bookmarks");
		clickLinkWithText("Bookmarks");

		assertTextPresent("Bookmarks in Demonstration Space");
		assertLinkNotPresentWithText("Bookmarks in Demonstration Space");
		assertLinkPresentWithText("Links for Demonstration Space");
		assertTextPresent("There are no bookmarks to display.");
		assertLinkPresentWithText("Bookmark in Confluence");
		assertLinkPresentWithText("Bookmark RSS Feed");

		clickLinkWithText("Links for Demonstration Space");

		assertLinkPresentWithText("Bookmarks in Demonstration Space");
		assertTextPresent("Links for Demonstration Space");
		assertLinkNotPresentWithText("Links for Demonstration Space");
		assertTextPresent("There are no bookmarks to display.");
		assertLinkPresentWithText("Bookmark in Confluence");
		assertLinkPresentWithText("Bookmark RSS Feed");
	}

	public void testCreateBookmark() throws UnsupportedEncodingException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, null);

		assertLinkNotPresentWithText("Bookmarks in " + testSpaceName + " space");
		assertLinkPresentWithText("Links for " + testSpaceName);
		assertLinkPresentWithText(bookmarkTitle);
		assertLinkPresent("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);
		assertLinkPresent("socialbookmark-remove-" + testSpaceName + ":" + bookmarkTitle);

		/* See the bookmarks for the test space */
		clickLinkWithText("Links for " + testSpaceName);

		assertTextPresent("There are no bookmarks to display.");

		removeBookmark(testSpaceKey, bookmarkTitle);
	}

	public void testCreateBookmarkWithDuplicatePageTitleInSpace()
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";
		PageHelper pageHelper = getPageHelper();
		SpaceHelper spaceHelper = getSpaceHelper(testSpaceKey);

		/* Create the page to be duplicated */
		pageHelper.setSpaceKey(testSpaceKey);
		pageHelper.setTitle("Google");
		pageHelper.setContent("[Google|http://www.google.com/]");
		assertTrue(pageHelper.create());


		assertTrue(spaceHelper.read());

		/* Try to create a bookmark with a title is not unique in the space */
		gotoPage("/plugins/socialbookmarking/updatebookmark.action?spaceKey=" + testSpaceKey + "&redirect=spacebookmarks");

		setWorkingForm("bookmarkForm");

		setTextField("title", bookmarkTitle);
		setTextField("url", bookmarkLocation);
		selectOption("spaceKey", spaceHelper.getName());
		setTextField("description", bookmarkDescription);

		submit("save");

		assertTextPresent("A page with the specified title already exists in the selected space.");

		assertTrue(pageHelper.delete());
	}

	public void testCreateBookmarkForSpace() throws UnsupportedEncodingException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, "for_" + testSpaceKey);

		assertLinkNotPresentWithText("Bookmarks in " + testSpaceName + " space");
		assertLinkPresentWithText("Links for " + testSpaceName);
		assertLinkPresentWithText(bookmarkTitle);
		assertLinkPresent("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);
		assertLinkPresent("socialbookmark-remove-" + testSpaceName + ":" + bookmarkTitle);

		/* See the bookmarks for the test space */
		clickLinkWithText("Links for " + testSpaceName);

		assertLinkPresentWithText("Bookmarks in " + testSpaceName + " space");
		assertLinkNotPresentWithText("Links for " + testSpaceName);
		assertLinkPresentWithText(bookmarkTitle);
		assertLinkPresent("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);
		assertLinkPresent("socialbookmark-remove-" + testSpaceName + ":" + bookmarkTitle);

		removeBookmark(testSpaceKey, bookmarkTitle);
	}

	public void testReadBookmarksInSpaceRssFeedWhenThereIsNoBookmarkInTheSpace() throws SAXException, FeedException, IOException
	{
		SyndFeedInput syndFeedInput;
		SyndFeed syndFeed;

		/* Verify that RSS feed returns no entries when there is no bookmarks for the space */
		gotoPage("/spaces/viewspacesummary.action?key=" + testSpaceKey);
		clickLinkWithText("Bookmarks");
		clickLink("socialbookmark-rss-" + testSpaceKey);

		syndFeedInput = new SyndFeedInput(false);
		syndFeed = syndFeedInput.build(new StringReader(getPageSource()));
		assertEquals("Bookmark RSS Feed", syndFeed.getTitle());
		assertEquals("Recent bookmarks in " + testSpaceName, syndFeed.getDescription());

		assertEquals(Collections.EMPTY_LIST, syndFeed.getEntries());
	}

	public void testReadBookmarksInSpaceRssFeed() throws SAXException, FeedException, IOException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";

		SyndFeedInput syndFeedInput;
		SyndFeed syndFeed;
		List feedEntries;
		SyndEntry syndEntry;
		List categories;
		SyndCategoryImpl syndCategory;
		String entryContent;
		Matcher matcher;

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, "label-01");

		clickLink("socialbookmark-rss-" + testSpaceKey);

		/* Validate RSS feed */
		syndFeedInput = new SyndFeedInput(false);
		syndFeed = syndFeedInput.build(new StringReader(getPageSource()));
		assertEquals("Bookmark RSS Feed", syndFeed.getTitle());
		assertEquals("Recent bookmarks in " + testSpaceName, syndFeed.getDescription());

		feedEntries = syndFeed.getEntries();
		assertEquals(1, feedEntries.size());

		syndEntry = (SyndEntry) feedEntries.get(0);

		entryContent = syndEntry.getDescription().getValue();

		/* Check if edit bookmark URL is based on the base URL */
		assertTrue((matcher = getEditBookmarkPattern(testSpaceName, bookmarkTitle).matcher(entryContent)).find());
		assertTrue(matcher.group(1).matches(baseUrl + "/plugins/socialbookmarking/updatebookmark.action\\?bookmarkPageId=\\d+&redirect=view&fromPageId=&returnUserToBookmarkPage="));

		/* Check if the remove bookmark URL is based on the base URL */
		assertTrue((matcher = getRemoveBookmarkPattern(testSpaceName, bookmarkTitle).matcher(entryContent)).find());
		assertTrue(matcher.group(1).matches(baseUrl + "/pages/removepage.action\\?pageId=\\d+"));

		/* Check if the view bookmark URL is based on the base URL */
		assertTrue((matcher = getViewBookmarkPattern(testSpaceName, bookmarkTitle).matcher(entryContent)).find());
		assertTrue(matcher.group(1).matches(baseUrl + "/pages/viewpage.action\\?pageId=\\d+"));


		assertEquals(bookmarkTitle, syndEntry.getTitle());
		assertEquals("Admin", syndEntry.getAuthor());

		categories = syndEntry.getCategories();
		assertEquals(1, categories.size());

		syndCategory = (SyndCategoryImpl) categories.get(0);
		assertEquals("label-01", syndCategory.getName());
	}

	public void testReadBookmarksForSpaceRssFeedWhenThereIsNoBookmarkForTheSpace() throws SAXException, FeedException, IOException
	{
		SyndFeedInput syndFeedInput;
		SyndFeed syndFeed;

		/* Verify that RSS feed returns no entries when there is no bookmarks for the space */
		gotoPage("/spaces/viewspacesummary.action?key=" + testSpaceKey);
		clickLinkWithText("Bookmarks");
		clickLinkWithText("Links for " + testSpaceName);
		clickLink("socialbookmark-rss-" + testSpaceKey);

		syndFeedInput = new SyndFeedInput(false);
		syndFeed = syndFeedInput.build(new StringReader(getPageSource()));
		assertEquals("Bookmark RSS Feed", syndFeed.getTitle());
		assertEquals("Recent bookmarks with labels for_" + testSpaceKey, syndFeed.getDescription());

		assertEquals(Collections.EMPTY_LIST, syndFeed.getEntries());
	}

	private long getDotBookmarksPageIdInSpace(String spaceKey)
	{
		PageHelper pageHelper = getPageHelper();

		pageHelper.setSpaceKey(spaceKey);
		pageHelper.setTitle(".bookmarks");

		return pageHelper.findBySpaceKeyAndTitle();
	}

	/**
	 * <a href="http://developer.atlassian.com/jira/browse/MARK-94">http://developer.atlassian.com/jira/browse/MARK-94</a>
	 */
	public void testBookmarksRssFeedInDotBookmarksPage() throws IOException, FeedException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";
		SyndFeedInput syndFeedInput;

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, "for_" + testSpaceKey);
		gotoPage("/pages/viewpage.action?pageId=" + getDotBookmarksPageIdInSpace(testSpaceKey));

		clickLinkWithImage("feed-icon-16x16.png");

		syndFeedInput = new SyndFeedInput(false);
		assertEquals(1, syndFeedInput.build(new StringReader(getPageSource())).getEntries().size());
	}

	public void testReadBookmarksForSpaceRssFeed() throws SAXException, FeedException, IOException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";
		SyndFeedInput syndFeedInput;
		SyndFeed syndFeed;
		List feedEntries;
		SyndEntry syndEntry;
		List categories;
		SyndCategoryImpl syndCategory;
		Matcher matcher;
		String entryContent;


		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, "for_" + testSpaceKey);
		clickLinkWithText("Links for " + testSpaceName);
		clickLink("socialbookmark-rss-" + testSpaceKey);

		/* Validate RSS feed */
		syndFeedInput = new SyndFeedInput(false);
		syndFeed = syndFeedInput.build(new StringReader(getPageSource()));
		assertEquals("Bookmark RSS Feed", syndFeed.getTitle());
		assertEquals("Recent bookmarks with labels for_" + testSpaceKey, syndFeed.getDescription());

		feedEntries = syndFeed.getEntries();
		assertEquals(1, feedEntries.size());

		syndEntry = (SyndEntry) feedEntries.get(0);
		assertEquals(bookmarkTitle, syndEntry.getTitle());
		assertEquals("Admin", syndEntry.getAuthor());

		entryContent = syndEntry.getDescription().getValue();

		/* Check if edit bookmark URL is based on the base URL */
		assertTrue((matcher = getEditBookmarkPattern(testSpaceName, bookmarkTitle).matcher(entryContent)).find());
		assertTrue(matcher.group(1).matches(baseUrl + "/plugins/socialbookmarking/updatebookmark.action\\?bookmarkPageId=\\d+&redirect=view&fromPageId=&returnUserToBookmarkPage="));

		/* Check if the remove bookmark URL is based on the base URL */
		assertTrue((matcher = getRemoveBookmarkPattern(testSpaceName, bookmarkTitle).matcher(entryContent)).find());
		assertTrue(matcher.group(1).matches(baseUrl + "/pages/removepage.action\\?pageId=\\d+"));

		/* Check if the view bookmark URL is based on the base URL */
		assertTrue((matcher = getViewBookmarkPattern(testSpaceName, bookmarkTitle).matcher(entryContent)).find());
		assertTrue(matcher.group(1).matches(baseUrl + "/pages/viewpage.action\\?pageId=\\d+"));

		categories = syndEntry.getCategories();
		assertEquals(1, categories.size());

		syndCategory = (SyndCategoryImpl) categories.get(0);
		assertEquals("for_" + testSpaceKey, syndCategory.getName());
	}

	public void testEditBookmark() throws SAXException, UnsupportedEncodingException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";
		String bookmarkTitleEdited = bookmarkTitle + " (Edited)";
		String bookmarkDescriptionEdited = bookmarkDescription + " (Edited)";
		String linkElementId;

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, "label-01");

		/* Check if bookmark has label set. */
		assertLinkPresentWithText("label-01");

		clickLinkWithText("Edit");
		assertTextPresent("Update Bookmark");

		setWorkingForm("bookmarkForm");
		setTextField("title", bookmarkTitleEdited);
		setTextField("url", "http://www.google.com.my");
		setTextField("description", bookmarkDescriptionEdited);
		setTextField("labelsString", "label-01 label-02");

		submit("save");

		linkElementId = "socialbookmark-" + testSpaceName + ":" + bookmarkTitleEdited;
		assertLinkPresent(linkElementId);

		assertEquals("http://www.google.com.my", getElementAttributByXPath("//a[@id='" + linkElementId + "']", "href"));

		assertLinkPresentWithText(bookmarkTitleEdited);
		assertTextPresent(bookmarkDescriptionEdited);
		/* Check if bookmark has its labels updated. */
		assertLinkPresentWithText("label-01");
		assertLinkPresentWithText("label-02");


		removeBookmark(testSpaceKey, bookmarkTitleEdited);
	}

	public void testEditBookmarkWithDuplicatePageTitleInSpace() throws UnsupportedEncodingException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";
		PageHelper pageHelper = getPageHelper();

		/* Create the page to be duplicated */
		pageHelper.setSpaceKey(testSpaceKey);
		pageHelper.setTitle("Googled Edited");
		pageHelper.setContent("[Google|http://www.google.com/]");
		assertTrue(pageHelper.create());

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, null);
		clickLink("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);

		setWorkingForm("bookmarkForm");
		setTextField("title", pageHelper.getTitle());
		submit("save");

		assertTextPresent("A page with the specified title already exists in the selected space.");

		assertTrue(pageHelper.delete());
	}

	/**
	 * <a href="http://developer.atlassian.com/browse/MARK-68">MARK-68</a>.
	 */
	public void testXssVulnerabilityInLabelsFieldDuringBookmarkCreation()
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";

		gotoPage("/plugins/socialbookmarking/updatebookmark.action?spaceKey=" + testSpaceKey + "&redirect=spacebookmarks");


		setWorkingForm("bookmarkForm");

		setTextField("title", bookmarkTitle);
		setTextField("url", bookmarkLocation);
		setTextField("description", bookmarkDescription);
		setTextField("labelsString", "><script>alert('hello');</script><span class=");

		submit("save");

		assertTextPresent("The labels you have entered contain invalid characters (" + LabelParser.getInvalidCharactersAsString() + ").");
	}

	/**
	 * <a href="https://studio.plugins.atlassian.com/browse/MARK-104">MARK-104</a>
	 */
	public void testXssVulnerabilityInUrlFieldDuringBookmarkCreation() throws IOException {
		String bookmarkTitle = "Xss";
		String bookmarkLocation = "http://foo.com\"><script>alert(1)</script><?";
		String bookmarkDescription = "Xss Bookmark";

		gotoPage("/plugins/socialbookmarking/updatebookmark.action?spaceKey=" + testSpaceKey + "&redirect=spacebookmarks");

		setWorkingForm("bookmarkForm");
		setTextField("title", bookmarkTitle);
		setTextField("url", bookmarkLocation);
		setTextField("description", bookmarkDescription);

		submit("save");

		final String linkId = "socialbookmark-" + GeneralUtil.htmlEncode(testSpaceName) + ":" + GeneralUtil.htmlEncode(bookmarkTitle);
		// final String href = getElementAttributByXPath("//a[@id='"+linkId+"']", "href");
		// href is decoded so can't be used in this test. We need the raw html source
		assertTrue("There should be an htmlEncoded URL as href attr.", this.getPageSource().contains("href=\"" + GeneralUtil.htmlEncode(bookmarkLocation)));
	}

	/**
	 * <a href="http://developer.atlassian.com/browse/MARK-68">MARK-68</a>.
	 */
	public void testXssVulnerabilityInLabelsFieldDuringBookmarkEdit() throws UnsupportedEncodingException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, StringUtils.EMPTY);

		clickLink("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);


		setWorkingForm("bookmarkForm");
		setTextField("labelsString", "><script>alert('hello');</script><span class=");
		submit("save");

		assertTextPresent("The labels you have entered contain invalid characters (" + LabelParser.getInvalidCharactersAsString() + ").");
	}

    public void testScriptBookmarksCannotBeCreated()
    {
        gotoPage("/plugins/socialbookmarking/updatebookmark.action?spaceKey=" + testSpaceKey + "&redirect=spacebookmarks");

		setWorkingForm("bookmarkForm");
		setTextField("title", "Javascript Attack");
		setTextField("url", "javascript:alert('oh hi')");
        submit("save");
        assertTextPresent("You cannot save script-based bookmarks.");

        setWorkingForm("bookmarkForm");
		setTextField("title", "VB Attack");
		setTextField("url", "vbscript:alert('oh hi')");
        submit("save");
        assertTextPresent("You cannot save script-based bookmarks.");
    }

	public void testViewBookmark() throws SAXException, UnsupportedEncodingException
	{
		SpaceHelper spaceHelper = getSpaceHelper(testSpaceKey);
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";
		String label = "label0-01";

		assertTrue(spaceHelper.read());

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, label);

		clickLink("socialbookmark-view-" + spaceHelper.getName() + ":" + bookmarkTitle);

		assertLinkPresentWithText(bookmarkTitle);
		assertLinkPresent("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);
		assertLinkPresent("socialbookmark-remove-" + testSpaceName + ":" + bookmarkTitle);

		/* We expect to links with the text "label-01" - one from page view and one rendered by the {bookmark} */
		assertElementPresentByXPath("//div[@id='content']/div[@class='wiki-content']//div[@class='pagesubheading']//span[@id='labelsList']/a[text()='" + label + "']");
		assertElementPresentByXPath("//div[@id='content']//div[@id='labelsInfo']//span[@id='labelsList']//a[text()='" + label + "']");

		removeBookmark(testSpaceKey, bookmarkTitle);
	}

	public void testLinkToLabelIsValidAtSpaceBookmarkTab() throws UnsupportedEncodingException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, "label-01");
		clickLinkWithText("label-01");
	}

	public void testLinkToLabelIsValidAtBookmarkView() throws UnsupportedEncodingException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, "label-01");
		clickLinkWithText("View Bookmark Page");


		clickLinkWithText("label-01", 0);
	}

	private File copyMark79SiteExport() throws IOException
	{
		InputStream in = null;
		OutputStream out = null;

		try
		{
			File copyMark79SiteExportCopy = File.createTempFile("socialbookmarking-", ".zip");

			in = getClass().getClassLoader().getResourceAsStream("mark-79-site-export.zip");
			out = new BufferedOutputStream(new FileOutputStream(copyMark79SiteExportCopy));

			IOUtils.copy(in, out);

			return copyMark79SiteExportCopy;
		}
		finally
		{
			IOUtils.closeQuietly(out);
			IOUtils.closeQuietly(in);
		}
	}

	public void testBookmarkParentPageNotCreatedByUnpermittedAnonymousUser() throws IOException
	{
		File copyMark79SiteExportCopy = null;

		try
		{
			ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();

			copyMark79SiteExportCopy = copyMark79SiteExport();
			confluenceWebTester.restoreData(copyMark79SiteExportCopy);

			logout(); /* Become anonymous user */
			gotoPage("/plugins/socialbookmarking/updatebookmark.action?title=SomeTitle&url=SomeURL&bookmarkPageId=&savePage=true&redirect=bookmarkurl&description=Something&labelsString=&save=Save&spaceKey=TST");

			loginAsAdmin(); /* Become admin */
			gotoPage("/pages/listpages-dirview.action?key=TST");

			assertLinkNotPresentWithText(".bookmarks"); /* Make sure the page is not created */
		}
		finally
		{
			if (null != copyMark79SiteExportCopy)
			{
				assertTrue(copyMark79SiteExportCopy.delete());
			}
		}
	}

	public void testCancelOutOfBookmarkCreation()
	{
		gotoPage("/spaces/space-bookmarks.action?spaceKey=" + testSpaceKey);
		clickLink("socialbookmark-add-bookmark-link");

		setWorkingForm("bookmarkForm");
		submit("cancel");

		assertTitleEquals("Space Bookmarks - Test Space - Confluence");
	}

	public void testCancelOutOfBookmarkCreationViaBookmarklet() throws IOException
	{
		final String dashboardUrl = getConfluenceWebTester().getBaseUrl() + "/dashboard.action";

		gotoPage("/plugins/socialbookmarking/updatebookmark.action?url=" + URLEncoder.encode(dashboardUrl, "UTF-8") + "&title=Dashboard - Confluence&description=");

		setWorkingForm("bookmarkForm");
		submit("cancel");

		assertTitleEquals("Dashboard - Confluence");
	}

	public void testSpaceKeysUrlEncodedInBookmarkParentPage() throws UnsupportedEncodingException
	{
		final UserHelper userHelper = getUserHelper();
		final PageHelper pageHelper;

		userHelper.setName("john doe"); /* Create a user with a space in the name */
		userHelper.setFullName("John Doe");
		userHelper.setEmailAddress("jdoe@confluence");
		userHelper.setPassword("jdoe");

		assertTrue(userHelper.create());

		logout();
		login(userHelper.getName(), userHelper.getPassword());

		/* Create a personal space */
		gotoPage("/spaces/createpersonalspace.action");
		setWorkingForm("create-personal-space-form");
		submit();

		createBookmarkInPersonalSpace("Google", "http://www.google.com", userHelper, "Google bookmark.", StringUtils.EMPTY);

		logout();
		loginAsAdmin();

		pageHelper = getPageHelper();
		pageHelper.setSpaceKey("~" + userHelper.getName());
		pageHelper.setTitle(".bookmarks");

		gotoPage("/pages/viewpage.action?pageId=" + pageHelper.findBySpaceKeyAndTitle());

		assertLinkPresentWithText("Bookmarks in " + userHelper.getFullName());
		assertLinkPresentWithText("Links for " + userHelper.getFullName());
	}

	public void testBookmarksShownForPersonalSpacesWithSpaceInTheKey() throws UnsupportedEncodingException
	{
		final UserHelper userHelper = getUserHelper();
		final PageHelper pageHelper;

		userHelper.setName("john doe"); /* Create a user with a space in the name */
		userHelper.setFullName("John Doe");
		userHelper.setEmailAddress("jdoe@confluence");
		userHelper.setPassword("jdoe");

		assertTrue(userHelper.create());

		logout();
		login(userHelper.getName(), userHelper.getPassword());

		/* Create a personal space */
		gotoPage("/spaces/createpersonalspace.action");
		setWorkingForm("create-personal-space-form");
		submit();

		createBookmarkInPersonalSpace("Google", "http://www.google.com", userHelper, "Google bookmark.", StringUtils.EMPTY);

		pageHelper = getPageHelper();
		pageHelper.setSpaceKey("~" + userHelper.getName());
		pageHelper.setTitle(".bookmarks");

		gotoPage("/pages/viewpage.action?pageId=" + pageHelper.findBySpaceKeyAndTitle());

		assertLinkPresentWithText("Google");
	}

	public void testBookmarkEmoticonInDescriptionRendered() throws UnsupportedEncodingException
	{
		String pageTitle = "testBookmarkEmoticonInDescriptionRendered";

        PageHelper dotBookmarksHelper = getPageHelper();
        dotBookmarksHelper.setSpaceKey(testSpaceKey);
        dotBookmarksHelper.setTitle(".bookmarks");
        assertTrue(dotBookmarksHelper.create());

        PageHelper bookmarkPageHelper = getPageHelper();
        bookmarkPageHelper.setSpaceKey(testSpaceKey);
        bookmarkPageHelper.setTitle(pageTitle);
        bookmarkPageHelper.setParentId(dotBookmarksHelper.getId());
        bookmarkPageHelper.setContent("{bookmark:url=http://www.atlassian.com/}:){bookmark}");
        assertTrue(bookmarkPageHelper.create());

		gotoPage("/spaces/space-bookmarks.action?spaceKey=" + testSpaceKey);
		assertElementPresentByXPath("//div[@class='description']//img[@class='emoticon emoticon-smile']");
	}

	void flushMailQueue()
	{
		try
		{
			gotoPageWithEscalatedPrivileges("/admin/console.action");
			clickLinkWithText("Mail Queue");
			clickLinkWithText("Flush Mail Queue");
		}
		finally
		{
			dropEscalatedPrivileges();
		}
	}

	private void initDumbsterConfig() throws IOException
	{
		InputStream in = null;

		try
		{
			dumbsterConfig = new Properties();
			in = getClass().getClassLoader().getResourceAsStream("dumbster.properties");

			dumbsterConfig.load(in);
		}
		finally
		{
			IOUtils.closeQuietly(in);
		}
	}

	private long addMailServer()
	{
		MailServerHelper mailServerHelper = getMailServerHelper();

		mailServerHelper.setName("Dumbster");
		mailServerHelper.setAddress("localhost:" + getDumbsterPort());
		mailServerHelper.setFromAddress(getConfluenceWebTester().getCurrentUserName() + "@atlassian.com");

		assertTrue(mailServerHelper.create());

		return mailServerHelper.getId();
	}

	private int getDumbsterPort()
	{
		return Integer.parseInt(dumbsterConfig.getProperty("smtp.port"));
	}

	private void deleteMailServer(long mailServerId)
	{
		assertTrue(getMailServerHelper(mailServerId).delete());
	}

	private void waitForMail()
	{
		waitForMail(DEFAULT_WAIT);
	}

	private void waitForMail(long maxWaitInMillis)
	{
		flushMailQueue();
		try
		{
			Thread.sleep(maxWaitInMillis <= 0 ? DEFAULT_WAIT : maxWaitInMillis);
		}
		catch (InterruptedException ie)
		{
			fail("Mail wait sleep interrupted.");
		}
	}

    // Commenting out since there is a change setting up mail server in Confluence 5.0. Fix in later date.
//	public void testBookmarkLinkIsValidInNotification() throws IOException
//	{
//		String bookmarkTitle = "Google";
//		String bookmarkLocation = "http://www.google.com/";
//		String bookmarkDescription = "Google Bookmark";
//
//		long mailServerId = 0;
//		SimpleSmtpServer simpleSmtpServer = null;
//
//		try
//		{
//			initDumbsterConfig();
//			mailServerId = addMailServer();
//			simpleSmtpServer = SimpleSmtpServer.start(getDumbsterPort());
//
//			//Start watching the space for notification
//			gotoPage("/spaces/viewspacesummary.action?key=" + testSpaceKey);
//			clickLink("space-watching");
//
//			//Say that I want to get notifications of the things I did
//			gotoPage("/users/viewmyemailsettings.action");
//			setWorkingForm("editemailsettingsform");
//			submit();
//			setWorkingForm("editemailsettingsform");
//			checkCheckbox("notifyForMyOwnActions");
//			submit("confirm");
//
//
//			createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, null);
//
//
//			flushMailQueue();
//			waitForMail();
//
//			assertEquals(2, simpleSmtpServer.getReceivedEmailSize());
//
//			Iterator messages = simpleSmtpServer.getReceivedEmail();
//			messages.next();
//
//			// We want the second message because the first is the notification for the .bookmarks page.
//			SmtpMessage smtpMessage = (SmtpMessage) messages.next();
//
//            // For debugging on CBAC
//            System.out.println(String.format("SMTP message body\n%s", smtpMessage.getBody()));
//
//            assertTrue(
//                    smtpMessage.getBody().matches(
//                            "(?s).*(<a\\ id=\"socialbookmark-Test Space:Google\"\\ .*href=\"http://www.google.com/\".*>Google</a>).*"
//                    )
//            );
//		}
//		finally
//		{
//			if (0 != mailServerId)
//				deleteMailServer(mailServerId);
//			if (null != simpleSmtpServer)
//				simpleSmtpServer.stop();
//		}
//	}

	public void testDefaultMaxBookmarksCountShownByBookmarksMacroIsNotIntegerMaxValue() throws UnsupportedEncodingException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, null);

		PageHelper pageHelper = getPageHelper();
		pageHelper.setSpaceKey(testSpaceKey);
		pageHelper.setTitle("testDefaultMaxBookmarksCountShownByBookmarksMacroIsNotIntegerMaxValue");
		pageHelper.setContent("{bookmarks}");

		assertTrue(pageHelper.create());

		gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
		assertTextPresent("The most recent bookmarks in " + testSpaceName);
		assertTextNotPresent(String.valueOf(Integer.MAX_VALUE));
	}

	public void testDefaultMaxBookmarksCountShownByBookmarksMacroWithLabelsParameterIsNotIntegerMaxValue() throws UnsupportedEncodingException
	{
		String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, "foobar");

		PageHelper pageHelper = getPageHelper();
		pageHelper.setSpaceKey(testSpaceKey);
		pageHelper.setTitle("testDefaultMaxBookmarksCountShownByBookmarksMacroWithLabelsParameterIsNotIntegerMaxValue");
		pageHelper.setContent("{bookmarks:labels=foobar}");

		assertTrue(pageHelper.create());

		gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
		assertTextPresent("The most recent bookmarks in " + testSpaceName);
		assertTextNotPresent(String.valueOf(Integer.MAX_VALUE));
	}

	public void testLabelsSuggestionWorksInConfluenceThreePointThree()
	{
		//        https://studio.plugins.atlassian.com/browse/MARK-106
		gotoPage("/plugins/socialbookmarking/updatebookmark.action?spaceKey=" + testSpaceKey + "&redirect=spacebookmarks");
		assertEquals(
				getElementAttributByXPath("//meta[@id='confluence-context-path']", "content"),
				getElementAttributByXPath("//fieldset[@class='hidden parameters']/input[@id='contextPath']", "value")
		);
	}

	public void testEditBookmarkCreatedByBookmarkMacro()
	{
		String bookmarkTitleEdited = "Gmail";
		String bookmarkDescriptionEdited = "Email from Google";
		String linkElementId;

		PageHelper pageWithBookmarkMacro = getPageHelper();
		pageWithBookmarkMacro.setSpaceKey(testSpaceKey);
		pageWithBookmarkMacro.setContent("{bookmark:url=http://www.google.com/}{bookmark}");
		pageWithBookmarkMacro.setTitle("Google");
		assertTrue(pageWithBookmarkMacro.create());

		gotoPage("/pages/viewpage.action?pageId=" + pageWithBookmarkMacro.getId());
		assertLinkPresentWithText("Google");
		clickLinkWithExactText("Edit");
		assertTextPresent("Update Bookmark");
		assertTextFieldEquals("url", "http://www.google.com/");

		setWorkingForm("bookmarkForm");
		setTextField("title", bookmarkTitleEdited);
		setTextField("url", "http://www.gmail.com");
		setTextField("description", bookmarkDescriptionEdited);
		setTextField("labelsString", "label-01 label-02");

		submit("save");

		linkElementId = "socialbookmark-" + testSpaceName + ":" + bookmarkTitleEdited;
		assertLinkPresent(linkElementId);

		assertEquals("http://www.gmail.com", getElementAttributByXPath("//a[@id='" + linkElementId + "']", "href"));

		assertLinkPresentWithText(bookmarkTitleEdited);
		assertTextPresent(bookmarkDescriptionEdited);
		/* Check if bookmark has its labels updated. */
		assertLinkPresentWithText("label-01");
		assertLinkPresentWithText("label-02");
		clickLink("socialbookmark-remove-" + testSpaceName + ":" + bookmarkTitleEdited);
	}

	public void testParentNotChangedToBookmarksWhenCreatePageWithBookmarkMacro()
	{
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Google");
        pageHelper.setContent("{bookmark:url=http://www.google.com/}{bookmark}");
        assertTrue(pageHelper.create());

		gotoPage("/spaces/space-bookmarks.action?spaceKey=" + testSpaceKey);
		assertLinkNotPresentWithExactText("Google");
	}

    // https://studio.plugins.atlassian.com/browse/MARK-108
    public void testWikiLinkToUserProfileRenderedInBookmarkDescription() throws UnsupportedEncodingException
    {
		String pageTitle = "testWikiLinkToUserProfileRenderedInBookmarkDescription";

        PageHelper dotBookmarksHelper = getPageHelper();
        dotBookmarksHelper.setSpaceKey(testSpaceKey);
        dotBookmarksHelper.setTitle(".bookmarks");
        assertTrue(dotBookmarksHelper.create());

        PageHelper bookmarkPageHelper = getPageHelper();
        bookmarkPageHelper.setSpaceKey(testSpaceKey);
        bookmarkPageHelper.setTitle(pageTitle);
        bookmarkPageHelper.setParentId(dotBookmarksHelper.getId());
        bookmarkPageHelper.setContent("{bookmark:url=http://www.atlassian.com/}[admin|~admin]{bookmark}");
        assertTrue(bookmarkPageHelper.create());

        gotoPage("/spaces/space-bookmarks.action?spaceKey=" + testSpaceKey);
        assertEquals("admin", getElementTextByXPath("//div[@id='content']//div[@class='wiki-content']//a"));
    }

    // https://studio.plugins.atlassian.com/browse/MARK-109
    public void testUrlRenderedInBookmarkDescription() throws UnsupportedEncodingException
    {
		String pageTitle = "testUrlRenderedInBookmarkDescription";

        PageHelper dotBookmarksHelper = getPageHelper();
        dotBookmarksHelper.setSpaceKey(testSpaceKey);
        dotBookmarksHelper.setTitle(".bookmarks");
        assertTrue(dotBookmarksHelper.create());

        PageHelper bookmarkPageHelper = getPageHelper();
        bookmarkPageHelper.setSpaceKey(testSpaceKey);
        bookmarkPageHelper.setTitle(pageTitle);
        bookmarkPageHelper.setParentId(dotBookmarksHelper.getId());
        bookmarkPageHelper.setContent("{bookmark:url=http://www.atlassian.com/}http://www.atlassian.com/{bookmark}");
        assertTrue(bookmarkPageHelper.create());

        gotoPage("/spaces/space-bookmarks.action?spaceKey=" + testSpaceKey);
        assertEquals("http://www.atlassian.com/", getElementAttributByXPath("//div[@id='content']//div[@class='wiki-content']//a", "href"));
    }

    public void testReturnToSpaceBookmarkAfterEditFromSpaceBookmark() throws UnsupportedEncodingException
    {
    	String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";
		String bookmarkTitleEdited = "Gmail";

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, null);

		assertLinkPresent("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);
		editBookmark(bookmarkTitle, bookmarkTitleEdited);

		assertTitleEquals("Space Bookmarks - " + testSpaceName + " - Confluence");
		assertLinkPresentWithText(bookmarkTitleEdited);
    }

    public void testReturnToBookmarksPageAfterEditFromBookmarksPage() throws UnsupportedEncodingException
    {
    	String bookmarkTitle = "Google";
		String bookmarkLocation = "http://www.google.com/";
		String bookmarkDescription = "Google Bookmark";
		String bookmarkTitleEdited = "Gmail";
		String bookmarksPageTitle = "Bookmarks";

		createBookmark(bookmarkTitle, bookmarkLocation, testSpaceKey, bookmarkDescription, null);

		PageHelper pageWithBookmarksMacro = getPageHelper();
		pageWithBookmarksMacro.setSpaceKey(testSpaceKey);
		pageWithBookmarksMacro.setContent("{bookmarks}");
		pageWithBookmarksMacro.setTitle(bookmarksPageTitle);
		assertTrue(pageWithBookmarksMacro.create());

		gotoPage("/pages/viewpage.action?pageId=" + pageWithBookmarksMacro.getId());

		assertTitleEquals(bookmarksPageTitle + " - " + testSpaceName + " - Confluence");
		assertLinkPresent("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);
		editBookmark(bookmarkTitle, bookmarkTitleEdited);

		assertTitleEquals(bookmarksPageTitle + " - " + testSpaceName + " - Confluence");
		assertLinkPresentWithText(bookmarkTitleEdited);
    }

    public void testReturnToBookmarkPageAfterEditFromBookmarkPage()
	{
    	String bookmarkTitle = "Google";
		String bookmarkTitleEdited = "Gmail";

		PageHelper pageWithBookmarkMacro = getPageHelper();
		pageWithBookmarkMacro.setSpaceKey(testSpaceKey);
		pageWithBookmarkMacro.setContent("{bookmark:url=http://www.google.com/}{bookmark}");
		pageWithBookmarkMacro.setTitle(bookmarkTitle);
		assertTrue(pageWithBookmarkMacro.create());

		gotoPage("/pages/viewpage.action?pageId=" + pageWithBookmarkMacro.getId());
		assertTitleEquals(bookmarkTitle + " - " + testSpaceName + " - Confluence");

		assertLinkPresent("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);
		editBookmark(bookmarkTitle, bookmarkTitleEdited);

		assertTitleEquals(bookmarkTitleEdited + " - " + testSpaceName + " - Confluence");
		assertLinkPresentWithText(bookmarkTitleEdited);
	}

    private void editBookmark(String bookmarkTitle, String bookmarkTitleEdited)
    {
		clickLink("socialbookmark-edit-" + testSpaceName + ":" + bookmarkTitle);

		setWorkingForm("bookmarkForm");
		setTextField("title", bookmarkTitleEdited);
		setTextField("url", "http://www.gmail.com");
		setTextField("description", bookmarkTitleEdited);

		submit("save");
    }

    public void testShowViewMoreLinkIfThereIsMoreBookmark() throws UnsupportedEncodingException
    {
    	createBookmark("Google", "http://www.google.com/", testSpaceKey, "Google Bookmark", null);
		createBookmark("Gmail", "http://www.gmail.com/", testSpaceKey, "Gmail Bookmark", null);

		// create {bookmarks} page
		PageHelper pageHelper = getPageHelper();
		pageHelper.setSpaceKey(testSpaceKey);
		pageHelper.setTitle("Bookmarks");
		pageHelper.setContent("{bookmarks:max=1}");
		assertTrue(pageHelper.create());
		gotoPage("/pages/viewpage.action?pageId=" + pageHelper.findBySpaceKeyAndTitle());
		assertLinkPresentWithExactText("View More");
    }

    public void testNotShowViewMoreLinkIfThereIsNoBookmark() throws UnsupportedEncodingException
    {
    	createBookmark("Google", "http://www.google.com/", testSpaceKey, "Google Bookmark", null);
		createBookmark("Gmail", "http://www.gmail.com/", testSpaceKey, "Gmail Bookmark", null);

		// create {bookmarks} page
		PageHelper pageHelper = getPageHelper();
		pageHelper.setSpaceKey(testSpaceKey);
		pageHelper.setTitle("Bookmarks");
		pageHelper.setContent("{bookmarks:max=2}");
		assertTrue(pageHelper.create());
		assertLinkNotPresentWithExactText("View More");
    }

    public void testUpdateBookmarkFormXsrfProtected()
    {
        getConfluenceWebTester().assertResourceXsrfProtected("/plugins/socialbookmarking/doupdatebookmark.action");
    }

    public void testPersonalLabelParsedCorrectWhenCreatingBookmarks() throws UnsupportedEncodingException
    {
        String bookmarkPageTitle = "Google";
        String personalLabelName = "my:follow-up";

        createBookmark(bookmarkPageTitle, "http://www.google.com/", testSpaceKey, "Google Bookmark", personalLabelName);
        getIndexHelper().update();
        gotoPage(String.format("/label/%s/%s", testSpaceKey, URLEncoder.encode(personalLabelName, "UTF-8")));

        assertLinkPresentWithText(bookmarkPageTitle);
    }

    public void testShowPersonalSpaceBookmarks() throws UnsupportedEncodingException
    {
        gotoPage("/spaces/createpersonalspace.action");
        setWorkingForm("create-personal-space-form");
        submit();

        String adminPersonalSpaceKey = "~" + getConfluenceWebTester().getCurrentUserName();
        String bookmarkPageTitle = "Google";

        createBookmark(bookmarkPageTitle, "http://www.google.com/", adminPersonalSpaceKey, "Google Bookmark", null);

        PageHelper bookmarksPageHelper = getPageHelper();
        bookmarksPageHelper.setSpaceKey(testSpaceKey);
        bookmarksPageHelper.setTitle("testShowPersonalSpaceBookmarks");
        bookmarksPageHelper.setContent("{bookmarks:spaces=@personal}");
        assertTrue(bookmarksPageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + bookmarksPageHelper.getId());
        assertElementPresentByXPath(
                "//div[@class='bookmarkListContainer']//a[@id='socialbookmark-Admin:Google']"
        );
    }

    public void testShowGlobalSpaceBookmarks() throws UnsupportedEncodingException
    {
        String bookmarkPageTitle = "Google";
        createBookmark(bookmarkPageTitle, "http://www.google.com/", testSpaceKey, "Google Bookmark", null);

        PageHelper bookmarksPageHelper = getPageHelper();
        bookmarksPageHelper.setSpaceKey(testSpaceKey);
        bookmarksPageHelper.setTitle("testShowGlobalSpaceBookmarks");
        bookmarksPageHelper.setContent("{bookmarks:spaces=@global}");
        assertTrue(bookmarksPageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + bookmarksPageHelper.getId());
        assertElementPresentByXPath(
                "//div[@class='bookmarkListContainer']//a[@id='socialbookmark-Test Space:Google']"
        );
    }

    public void testShowAllSpaceBookmarks() throws UnsupportedEncodingException
    {
        gotoPage("/spaces/createpersonalspace.action");
        setWorkingForm("create-personal-space-form");
        submit();

        String adminPersonalSpaceKey = "~" + getConfluenceWebTester().getCurrentUserName();
        String personalSpaceBookmarkTitle = "Google";

        createBookmark(personalSpaceBookmarkTitle, "http://www.google.com/", adminPersonalSpaceKey, "Google Bookmark", null);

        String bookmarkPageTitle = "Google";
        createBookmark(bookmarkPageTitle, "http://www.google.com/", testSpaceKey, "Google Bookmark", null);

        PageHelper bookmarksPageHelper = getPageHelper();
        bookmarksPageHelper.setSpaceKey(testSpaceKey);
        bookmarksPageHelper.setTitle("testShowAllSpaceBookmarks");
        bookmarksPageHelper.setContent("{bookmarks:spaces=@all}");
        assertTrue(bookmarksPageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + bookmarksPageHelper.getId());
        assertElementPresentByXPath(
                "//div[@class='bookmarkListContainer']//a[@id='socialbookmark-Admin:Google']"
        );
        assertElementPresentByXPath(
                "//div[@class='bookmarkListContainer']//a[@id='socialbookmark-Test Space:Google']"
        );
    }
}
