package com.atlassian.confluence.plugins.socialbookmarking;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.themes.ThemeHelper;
import com.atlassian.user.User;
import com.sun.syndication.feed.synd.SyndFeed;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface BookmarkUtils
{
	String BOOKMARK_PARENT_PAGE = ".bookmarks";
	String URL_PROPERTY_KEY = "socialbookmarkingurl";
	String ISBOOKMARK_PROPERTY_KEY = "isbookmark";
	String FORLABEL_PREFIX = "for_";
	String SORT_CREATION = "creation";
	String SORT_CREATOR = "creator";
	String SORT_TITLE = "title";
	String SPACES_ALL = "@all";
	String SPACES_GLOBAL = "@global";
	String SPACES_PERSONAL = "@personal";
    
    boolean isSupportedContentType(ContentEntityObject contentEntityObject);

    /**
	 * Returns the {@code .bookmarks} page of the specified {@link com.atlassian.confluence.spaces.Space}
	 *
	 * @param space
	 * The {@link com.atlassian.confluence.spaces.Space} to get the {@link .bookmarks} page for
     * @param create
     * Flag whether to create the {@code .bookmarks} page if it does not exist
	 * @return
     * The {@link Page} representing the {@code .bookmarks} page.
	 */
    Page getBookmarkParent(Space space, boolean create);

    /**
	 * Returns a {@link Bookmark} object given a content ID
	 *
	 * @param contentId
     * The ID of the content to intepret as bookmark
	 * @return
     * A {@link Bookmark} object presenting the interpretation.
	 */
    public Bookmark getBookmark(long contentId);


	/**
	 * Returns a {@link Bookmark} object from a {@link ContentEntityObject}
	 *
	 * @param contentEntityObject
     * The {@link ContentEntityObject} to interpret as a bookmark
	 * @param user
     * The {@link User} to which the interpretation is done for
	 * @return
     * A {@link Bookmark} object presenting the interpretation.
     *
	 * @throws IllegalArgumentException
     * If ContentEntityObject is neither Page nor Draft
	 */
    Bookmark convertPageToBookmark(ContentEntityObject contentEntityObject, User user);

    Space getSpaceFromContentEntity(ContentEntityObject contentEntityObject);

    /**
	 * Returns bookmarks objects based on the given search criteria
	 *
	 * @param spaceKeys
     *            The space keys
	 * @param labels
	 *            String of labels to include, comma delimited
	 * @param creators
	 *            String of user names to search, comma delimited
	 * @param sortParams
	 *            String of sort parameters to return the bookmark list in.
	 *            Refer to static class variables with prefix SORT_
	 * @param max
	 *            maximum number of bookmarks to return in the list, a negative
	 *            value will return all bookmarks.
	 * @param pageNumber
	 * 			  indicate which batch of bookmarks to return in the list
	 * @param reverse
	 *            true and the bookmark sort order will be reversed.
	 * @return
     * A {@link Bookmarks} object representing matching bookmarks
	 */
    Bookmarks getBookmarkList(Set<String> spaceKeys, String labels, String creators, String sortParams, int max, int pageNumber, boolean reverse);


	/**
	 * Returns bookmarks objects based on the given search criteria
	 *
	 * @param spaceKeys
	 *            String of space keys to include, comma delimited
	 * @param labelsString
	 *            String of labelsString to include, comma delimited
	 * @param creators
	 *            String of user names to search, commar delimited
	 * @param sortParams
	 *            String of sort parameters to return the bookmark list in.
	 *            Refer to static class variables with prefix SORT_
	 * @param max
	 *            maximum number of bookmarks to return in the list, a negative
	 *            value will return all bookmarks.
	 * @param pageNumber
	 * 			  indicate which batch of bookmarks to return in the list
	 * @return
     * A {@link Bookmarks} object representing matching bookmarks
	 */
    Bookmarks getBookmarkList(Set<String> spaceKeys, String labelsString, String creators, String sortParams, int max, int pageNumber);

    /**
	 * Creates an RSS URL containing bookmarks
	 *
     * @param space
     * The containing space the URL will be shown in
	 * @param spaceKeys
     * A list of comma separated space keys. RSS feed will only show bookmark items from spaces with those keys.
	 * @param sortParams
     * Any of {@link #SORT_TITLE}, {@link #SORT_CREATOR} and {@link #SORT_CREATION}
	 * @param max
     * The maximum number of bookmarks to display in the feed
	 * @param reverse
     * The order of the bookmarks in the feed. Set {@code false} if reverse order is desired
	 * @param displayParams
     * Set of flags that will determine how the bookmarks are rendered. See {@link com.atlassian.confluence.plugins.socialbookmarking.macros.AbstractBookmarksMacro}.
	 * @return
     * The URL to the RSS feed of bookmarks
	 */
    String createRssUrl(Space space, String spaceKeys, String labels, String sortParams, int max, boolean reverse, Map<?, ?> displayParams);

    /**
	 * Creates a {@link com.sun.syndication.feed.synd.SyndFeed} representing an RSS feed of the specified list of bookmarks
	 *
	 * @param bookmarks
     * The bookmarks to make an RSS feed off
	 * @param helper
	 * @param params
	 * The parameters when rendering the RSS feed (show/hide title). See {@link com.atlassian.confluence.plugins.socialbookmarking.macros.AbstractBookmarksMacro#getParams(java.util.Map, java.util.Map)}
	 * @return
     * A {@link com.sun.syndication.feed.synd.SyndFeed} representing the RSS feed of the bookmarks
	 */
    SyndFeed createSyndFeed(List<Bookmark> bookmarks, ThemeHelper helper, Map<String, String> params);

    /**
     * Only support Bookmarks using HTTP, HTTPS, and FILE protocols
     *
     * @param url
     * The url entered by the user to be linked to in the bookmark.
     *
     * @return
     * true if the url starts with valid protocols (HTTPS, HTTP, FILE), false otherwise.
     */
    public boolean isValidProtocol(String url);
}
