package com.atlassian.confluence.plugins.socialbookmarking.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ListBuilder;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.core.actions.RssDescriptor;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmark;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmarks;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceType;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.renderer.v2.RenderMode;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BookmarksMacro extends AbstractBookmarksMacro
{
    private static final Logger LOG = LoggerFactory.getLogger(BookmarksMacro.class);

    private WebResourceManager webResourceManager;

    private VelocityHelperService velocityHelperService;

    public BookmarksMacro(BookmarkUtils bookmarkUtils, SpaceManager spaceManager, LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, WebResourceManager webResourceManager, VelocityHelperService velocityHelperService)
    {
        super(bookmarkUtils, spaceManager, localeManager, i18NBeanFactory);
        setWebResourceManager(webResourceManager);
        setVelocityHelperService(velocityHelperService);
    }

    @SuppressWarnings("unused")
    public BookmarksMacro()
    {
        this(null, null, null, null, null, null); 
    }

    public void setWebResourceManager(WebResourceManager webResourceManager)
    {
        this.webResourceManager = webResourceManager;
    }

    public void setVelocityHelperService(VelocityHelperService velocityHelperService)
    {
        this.velocityHelperService = velocityHelperService;
    }
	
	public String execute(Map<String, String> params, String body, ConversionContext conversionContext)
    {
		ContentEntityObject contentObject = conversionContext.getEntity();
		
		// Labels
        String labelString = StringUtils.trim(
                StringUtils.defaultString(
                        params.get(LABELS_PARAM),
                        params.get(LABEL_PARAM)));
		
        List<String> spaceKeys = getSpaceKeys(contentObject, params);
        
        // Creators
        String creators = StringUtils.trim(
                StringUtils.defaultString(
                        params.get(CREATORS_PARAM),
                        params.get(CREATOR_PARAM)));
		
		// Sort String
		String sortString = StringUtils.trim(params.get(SORT_PARAM));
		
		// Max bookmarks, default is unlimited
		String maxString = StringUtils.trim(params.get(MAX_PARAM));
        Integer max = null;
        try {
			max = new Integer(maxString);
		} catch (NumberFormatException e) {
            if (LOG.isDebugEnabled())
                LOG.debug(String.format("Unable to parse \"" + MAX_PARAM + "\" (%s). Defaulting to unlimited", maxString));
        }
		
		

		String returnText = "";
		
		// We need a space key to do anything, it could still be null if macro
		// is rendered on the dashboard or something
		if (!spaceKeys.isEmpty()) {

            // reverse is false by default
            boolean reverse = BooleanUtils.toBoolean(params.get(REVERSE_PARAM));
            int pageNumber = 1;

            Bookmarks bookmarkList = bookmarkUtils.getBookmarkList(new LinkedHashSet<String>(spaceKeys), labelString, creators, sortString, null == max ? Integer.MAX_VALUE : max, pageNumber, reverse);
            List<Bookmark> bookmarks = bookmarkList.getBookmarkList();

			if (RenderContextOutputType.PREVIEW.equals(conversionContext.getOutputType()))
			{
				setBookmarksReadOnly(bookmarks);
			}
			
			Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();

            velocityContext = getParams(params, velocityContext);
            
            velocityContext.put(SPACES_PARAM, StringUtils.join(spaceKeys, ","));
            velocityContext.put(LABELS_PARAM, labelString);
            velocityContext.put(CREATORS_PARAM, creators);
            velocityContext.put(SORT_PARAM, sortString);
            
            velocityContext.put("nextPage", bookmarkList.isHasNextPage());
            velocityContext.put("bookmarks", bookmarks);

            //MARK-123; CONF-21215 - Only integers make sense for the max parameter. So we always override with the max
            // variable, even if it is null. Note: if we don't do this we will be vulnerable to XSS attacks in the max parameter.
            velocityContext.put("max", max);
            velocityContext.put("content", contentObject);
            // having "space" so the space name can be printed out in preview; 
            // $content.space.name in vm is not working if the page is not saved
            Space anchorSpace = spaceManager.getSpace(spaceKeys.iterator().next());
            velocityContext.put("space", anchorSpace);
            velocityContext.put("labelString", labelString);
            // MARK-116
            velocityContext.put("fromPageId", contentObject.getIdAsString());

            if (spaceKeys.size() > 1)
                velocityContext.put("spaceNames", StringUtils.join(getSpaceNames(spaceKeys), ", "));

            // If rss is to be shown, create a url for the rss action
			if((Boolean) velocityContext.get(SHOWRSS_PARAM)){
				velocityContext.put(
                        "rssUrl",
                        new RssDescriptor(
                                bookmarkUtils.createRssUrl(anchorSpace, StringUtils.join(spaceKeys, ","), labelString, sortString, null == max ? Integer.MAX_VALUE : max, reverse, getParams(params, new HashMap<String, Object>())),
                                getText("bookmark.rss.title"),
                                true
                        ).getRssHref()
                );
			}

            returnText = velocityHelperService.getRenderedTemplate("templates/plugins/socialbookmarking/bookmarklist.vm", velocityContext);
		}
		
		webResourceManager.requireResource("com.atlassian.confluence.plugins.socialbookmarking:more-bookmarks-web-resources");
		return returnText;
	}

    private Set<String> getSpaceNames(Collection<String> spaceKeys)
    {
        return new LinkedHashSet<String>(Collections2.transform(
                spaceKeys,
                new Function<String, String>()
                {
                    @Override
                    public String apply(String spaceKey)
                    {
                        return spaceManager.getSpace(spaceKey).getName();
                    }
                }
        ));
    }

    public RenderMode getBodyRenderMode() {
		
		return RenderMode.NO_RENDER;
	}

    public boolean hasBody()
    {
        return false;
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    /**
	 * Set the edit and remove permission to false. This is to make the bookmarks readonly so that the Edit / Remove
	 * links won't appear in the preview 
	 */
	private List<Bookmark> setBookmarksReadOnly(List<Bookmark> bookmarks)
	{
		for (Bookmark bookmark : bookmarks)
		{
			bookmark.setEditPermission(false);
			bookmark.setRemovePermission(false);
		}
		return bookmarks;
	}
	
    private List<String> getSpaceKeys(ContentEntityObject contentObject, Map params)
    {
        Set<String> spaceKeys = new LinkedHashSet<String>();
        String[] spaceKeysTokenized = StringUtils.split((String) params.get(SPACES_PARAM), ", ");

        if (null == spaceKeysTokenized || 0 == spaceKeysTokenized.length)
        {
            if (contentObject instanceof SpaceContentEntityObject)
            {
                spaceKeys.add(((SpaceContentEntityObject) contentObject).getSpaceKey());
            }
            else if (contentObject instanceof Draft)
            {
                spaceKeys.add(((Draft) contentObject).getDraftSpaceKey());
            }
            else if (contentObject instanceof Comment)
            {
                ContentEntityObject commentOwner = ((Comment) contentObject).getOwner();
                spaceKeys.addAll(getSpaceKeys(commentOwner, params));
            }
        }
        else
        {
            for (String spaceKey : spaceKeysTokenized)
            {
                Function<Space, String> spaceToSpaceKeyFunction = new Function<Space, String>()
                {
                    @Override
                    public String apply(Space space)
                    {
                        return space.getKey();
                    }
                };

                if (StringUtils.equals(spaceKey, "@all"))
                {
                    ListBuilder<Space> spacesForUser = spaceManager.getSpaces(SpacesQuery.newQuery().forUser(AuthenticatedUserThreadLocal.getUser()).build());

                    spaceKeys.addAll(
                            Collections2.transform(
                                    spacesForUser.getRange(0, spacesForUser.getAvailableSize()),
                                    spaceToSpaceKeyFunction
                            )
                    );

                    break;
                }
                else if (StringUtils.equals(spaceKey, "@personal"))
                {
                    ListBuilder<Space> spacesForUser = spaceManager.getSpaces(SpacesQuery.newQuery().forUser(AuthenticatedUserThreadLocal.getUser()).withSpaceType(SpaceType.PERSONAL).build());

                    spaceKeys.addAll(
                            Collections2.transform(
                                    spacesForUser.getRange(0, spacesForUser.getAvailableSize()),
                                    spaceToSpaceKeyFunction
                            )
                    );
                }
                else if (StringUtils.equals(spaceKey, "@global"))
                {
                    ListBuilder<Space> spacesForUser = spaceManager.getSpaces(SpacesQuery.newQuery().forUser(AuthenticatedUserThreadLocal.getUser()).withSpaceType(SpaceType.GLOBAL).build());

                    spaceKeys.addAll(
                            Collections2.transform(
                                    spacesForUser.getRange(0, spacesForUser.getAvailableSize()),
                                    spaceToSpaceKeyFunction
                            )
                    );
                }
            }

            spaceKeys.addAll(Arrays.asList(spaceKeysTokenized));
        }

        return new ArrayList<String>(
                Collections2.filter(
                        spaceKeys,
                        new Predicate<String>()
                        {
                            @Override
                            public boolean apply(String spaceKey)
                            {
                                return null != spaceManager.getSpace(spaceKey);
                            }
                        }
                )
        );
    }
	
}
