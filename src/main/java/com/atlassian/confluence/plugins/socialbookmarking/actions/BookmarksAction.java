package com.atlassian.confluence.plugins.socialbookmarking.actions;

import bucket.core.actions.PaginationSupport;
import com.atlassian.confluence.core.actions.RssDescriptor;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.plugins.socialbookmarking.Bookmark;
import com.atlassian.confluence.plugins.socialbookmarking.BookmarkUtils;
import com.atlassian.confluence.plugins.socialbookmarking.macros.AbstractBookmarksMacro;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAction;
import com.atlassian.confluence.spaces.actions.SpaceAware;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.xwork.results.RssResult;
import com.sun.syndication.feed.synd.SyndFeed;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class BookmarksAction extends AbstractSpaceAction implements SpaceAware, PageAware
{
    private static final Logger LOG = LoggerFactory.getLogger(BookmarksAction.class);
    
    static final String MODE_SPACEBOOKMARKS = "spacebookmarks";

    static final String MODE_BOOKMARKSFORSPACE = "bookmarksfor";

    private String labels;
    private String sort;
    private String max;
    private String creators;
    private boolean reverseSort;

    private Space space;
    private AbstractPage bookmarkPage;
    private String output;
    private String mode = MODE_SPACEBOOKMARKS;
    
    // Optional display options
    private boolean showTitle = true;
    private boolean showAuthor = true;
    private boolean showLabels = true;
    private boolean showDate = true;
    private boolean showDescription = true;
    private boolean showSpace = true;
    private boolean showComments = true;
    private boolean showEditLinks = true;
    private boolean showListHeader = true;
    
    private BookmarkUtils bookmarkUtils;

    /**
     * The default number of content to be displayed on each page.
     */
    public static final int ITEMS_PER_PAGE = 15;

    /**
     * Pagination support to assist in the display of the list of content associated with the label.
     */
    private PaginationSupport paginationSupport = new PaginationSupport(ITEMS_PER_PAGE);

    public Space getSpace()
    {
        return space;
    }

    public void setSpace(Space space)
    {
        this.space = space;
    }

    public boolean isSpaceRequired()
    {
        return true;
    }

    public boolean isViewPermissionRequired()
    {
        return true;
    }

    public AbstractPage getPage()
    {
        return bookmarkPage;
    }

    public void setPage(AbstractPage abstractPage)
    {
        bookmarkPage = abstractPage;
    }

    public boolean isPageRequired()
    {
        return false;
    }

    public boolean isLatestVersionRequired()
    {
        return true;
    }

    public String execute() throws Exception
    {
        // Initialize the pagination support with items
        getPaginationSupport().setItems(getBookmarks());

        return StringUtils.equals(RssResult.RSS, getOutput())
                ? RssResult.RSS
                : SUCCESS;
    }

    @SuppressWarnings("unused")
    public RssDescriptor getRssDescriptor()
    {
        return new RssDescriptor(
                getRssUrl(),
                getRssTitle(),
                true);
    }

    private String getRssTitle()
    {
        String spaceKey = getSpace().getKey();
        return !isForSpaceMode()
                ? new StringBuilder(getText("bookmark.rss.space")).append(" ").append(spaceKey).toString()
                : new StringBuilder(getText("bookmark.rss.forspace")).append(" ").append(spaceKey).toString();
    }

    private String getRssUrl()
    {
        String mode = getMode();
        boolean isNotForSpace = !isForSpaceMode();
        Space currentSpace = getSpace();

        return new StringBuilder(bookmarkUtils.createRssUrl(
                currentSpace,
                isNotForSpace ? getSpace().getKey() : BookmarkUtils.SPACES_ALL,
                isNotForSpace ? getLabels() : new StringBuilder(BookmarkUtils.FORLABEL_PREFIX).append(getSpace().getKey()).toString(),
                getSort(),
                getMaxBookmarksToList(),
                isReverseSort(),
                createDisplayParamMap()
        )).append("&mode=").append(GeneralUtil.urlEncode(mode))
        .toString();
    }

    public SyndFeed getSyndFeed() throws Exception
    {
        int maxBookmarksToList = getMaxBookmarksToList();
        SyndFeed feed = bookmarkUtils.createSyndFeed((List<Bookmark>) getPaginationSupport().getItems(), getHelper(), createDisplayParamMap());

        if (!isForSpaceMode())
        {
            Space bookmarkSpace = getSpace();
            if (0 >= maxBookmarksToList)
            {
                feed.setDescription(getText("bookmarks.inspace", new Object[] { bookmarkSpace.getName() }));
            }
            else
            {
                if (StringUtils.isBlank(getLabels()))
                {
                    feed.setDescription(getText("bookmarks.inspace.withcount", new Object[] { maxBookmarksToList, bookmarkSpace.getName() }));
                }
                else
                {
                    feed.setDescription(getText(
                            "bookmarks.inspace.withcountandlabels",
                            new Object[]{ maxBookmarksToList, bookmarkSpace.getName(), getLabels() }
                    ));
                }
            }
        }
        else
        {
            /* When feeds are requested using labels (no space keys) */
            if (0 >= maxBookmarksToList)
            {
                feed.setDescription(getText("bookmarks.forspace", new Object[] { getLabels() }));
            }
            else
            {
                feed.setDescription(getText("bookmarks.forspace.withcount", new Object[] { maxBookmarksToList, getLabels() }));
            }
        }

        return feed;
    }

    private Map<String, String> createDisplayParamMap()
    {
    	Map<String, String> params = new HashMap<String, String>();

    	params.put(AbstractBookmarksMacro.SHOWTITLE_PARAM, String.valueOf(isShowTitle()));
		params.put(AbstractBookmarksMacro.SHOWAUTHOR_PARAM, String.valueOf(isShowAuthor()));
		params.put(AbstractBookmarksMacro.SHOWLABELS_PARAM, String.valueOf(isShowLabels()));
		params.put(AbstractBookmarksMacro.SHOWDATE_PARAM, String.valueOf(isShowDate()));
		params.put(AbstractBookmarksMacro.SHOWDESCRIPTION_PARAM, String.valueOf(isShowDescription()));
		params.put(AbstractBookmarksMacro.SHOWSPACE_PARAM, String.valueOf(isShowSpace()));
		params.put(AbstractBookmarksMacro.SHOWCOMMENTS_PARAM, String.valueOf(isShowComments()));
		params.put(AbstractBookmarksMacro.SHOWEDIT_PARAM, String.valueOf(isShowEditLinks()));
		params.put(AbstractBookmarksMacro.SHOWLISTHEADER_PARAM, String.valueOf(isShowListHeader()));

        /* So that we know if we are in wiki mode or not. This allows us determine whether to use the base URL for URLs generated
         * in viewbookmark.vm later on.
         */
        if (StringUtils.equals(RssResult.RSS, getOutput()))
            params.put("output", RssResult.RSS);

        return params;
	}

    public PaginationSupport getPaginationSupport()
    {
        return paginationSupport;
    }

    private int getMaxBookmarksToList()
    {
        int maxBookmarksToList = -1;

        try
        {
            maxBookmarksToList = Integer.parseInt(getMax());
        }
        catch (NumberFormatException nfe)
        {
            LOG.debug(String.format("Unable to parse %d as integer", getMax()));
        }

        return maxBookmarksToList;
    }

    public List<Bookmark> getBookmarks()
    {
        AbstractPage bookmarkPage = getPage();
        List<Bookmark> bookmarks;

        if (null != bookmarkPage)
        {
            bookmarks = Arrays.asList(bookmarkUtils.getBookmark(bookmarkPage.getId()));
        }
        else
        {
            boolean isNotForSpace = !isForSpaceMode();
            bookmarks = bookmarkUtils.getBookmarkList(
                    new HashSet<String>(Arrays.asList(isNotForSpace ? getSpace().getKey() : BookmarkUtils.SPACES_ALL)),
                    isNotForSpace ? getLabels() : new StringBuilder(BookmarkUtils.FORLABEL_PREFIX).append(getSpace().getKey()).toString(),
                    getCreators(),
                    getSort(),
                    getMaxBookmarksToList(),
                    1,
                    isReverseSort()).getBookmarkList();
        }
        
        return bookmarks;
    }

    private boolean isForSpaceMode()
    {
        return StringUtils.equals(MODE_BOOKMARKSFORSPACE, getMode());
    }

    public void setStartIndex(int startIndex)
    {
        getPaginationSupport().setStartIndex(startIndex);
    }

    public String getOutput()
    {
        return output;
    }

    public void setOutput(String output)
    {
        this.output = output;
    }

    public String getMode()
    {
        return mode;
    }

    public void setMode(String mode)
    {
        this.mode = mode;
    }

    public String getLabels()
    {
        return labels;
    }

    public void setLabels(String labels)
    {
        this.labels = labels;
    }

    public String getSort()
    {
        return sort;
    }

    @SuppressWarnings("unused")
    public void setSort(String sort)
    {
        this.sort = sort;
    }

    public String getMax()
    {
        return max;
    }

    public void setMax(String max)
    {
        this.max = max;
    }

    public String getCreators()
    {
        return creators;
    }

    @SuppressWarnings("unused")
    public void setCreators(String creators)
    {
        this.creators = creators;
    }

    public boolean isReverseSort()
    {
        return reverseSort;
    }

    @SuppressWarnings("unused")
    public void setReverseSort(boolean reverseSort)
    {
        this.reverseSort = reverseSort;
    }

    public boolean isShowTitle()
    {
        return showTitle;
    }

    @SuppressWarnings("unused")
    public void setShowTitle(boolean showTitle)
    {
        this.showTitle = showTitle;
    }

    public boolean isShowAuthor()
    {
        return showAuthor;
    }

    @SuppressWarnings("unused")
    public void setShowAuthor(boolean showAuthor)
    {
        this.showAuthor = showAuthor;
    }

    public boolean isShowLabels()
    {
        return showLabels;
    }

    @SuppressWarnings("unused")
    public void setShowLabels(boolean showLabels)
    {
        this.showLabels = showLabels;
    }

    public boolean isShowDate()
    {
        return showDate;
    }

    @SuppressWarnings("unused")
    public void setShowDate(boolean showDate)
    {
        this.showDate = showDate;
    }

    public boolean isShowDescription()
    {
        return showDescription;
    }

    @SuppressWarnings("unused")
    public void setShowDescription(boolean showDescription)
    {
        this.showDescription = showDescription;
    }

    public boolean isShowSpace()
    {
        return showSpace;
    }

    @SuppressWarnings("unused")
    public void setShowSpace(boolean showSpace)
    {
        this.showSpace = showSpace;
    }

    public boolean isShowComments()
    {
        return showComments;
    }

    @SuppressWarnings("unused")
    public void setShowComments(boolean showComments)
    {
        this.showComments = showComments;
    }

    public boolean isShowEditLinks()
    {
        return showEditLinks;
    }

    @SuppressWarnings("unused")
    public void setShowEditLinks(boolean showEditLinks)
    {
        this.showEditLinks = showEditLinks;
    }

    public boolean isShowListHeader()
    {
        return showListHeader;
    }

    @SuppressWarnings("unused")
    public void setShowListHeader(boolean showListHeader)
    {
        this.showListHeader = showListHeader;
    }

    public void setBookmarkUtils(BookmarkUtils bookmarkUtils)
    {
        this.bookmarkUtils = bookmarkUtils;
    }
}
