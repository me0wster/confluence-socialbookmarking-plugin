package com.atlassian.confluence.plugins.socialbookmarking.conditions;

import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.conditions.BaseConfluenceCondition;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.themes.Theme;
import com.atlassian.confluence.themes.ThemeManager;
import com.atlassian.plugin.PluginAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BackwardsCompatibilitySpaceSideBarCondition extends BaseConfluenceCondition
{
    private static final Logger LOG = LoggerFactory.getLogger(BackwardsCompatibilitySpaceSideBarCondition.class);
    private static final String SIDEBAR_PLUGIN_KEY = "com.atlassian.confluence.plugins.confluence-space-ia";
    private PluginAccessor  pluginAccessor;
    private ThemeManager themeManager;

    @SuppressWarnings("unused")
    public void setPluginAccessor(PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    @SuppressWarnings("unused")
    public void setThemeManager(ThemeManager themeManager)
    {
        this.themeManager = themeManager;
    }

    @Override
    protected boolean shouldDisplay(WebInterfaceContext webInterfaceContext)
    {
        Space space = webInterfaceContext.getSpace();
        boolean pluginEnabled = pluginAccessor.isPluginEnabled(SIDEBAR_PLUGIN_KEY);
        boolean correctSpaceTheme = space != null && hasSpaceSideBar(space);
        return correctSpaceTheme && pluginEnabled;
    }

    private boolean hasSpaceSideBar(Space space)
    {
        Theme spaceTheme = themeManager.getSpaceTheme(space.getKey());
        try
        {
            Method hasSpaceSideBarMethod = spaceTheme.getClass().getMethod("hasSpaceSideBar");
            return (Boolean) hasSpaceSideBarMethod.invoke(spaceTheme);
        }
        catch (NoSuchMethodException noHasSideBarMethod)
        {
            LOG.debug("Theme object does not have hasSideBar() method. Assuming we dont't have Space IA.", noHasSideBarMethod);
        }
        catch (IllegalAccessException hasSideBarMethodNotVisible)
        {
            LOG.error("Theme.hasSideBar() cannot be called.", hasSideBarMethodNotVisible);
        }
        catch (InvocationTargetException hasSideBarMethodFubared)
        {
            LOG.error("Theme.hasSideBar() failed.", hasSideBarMethodFubared);
        }
        return false;
    }
}
