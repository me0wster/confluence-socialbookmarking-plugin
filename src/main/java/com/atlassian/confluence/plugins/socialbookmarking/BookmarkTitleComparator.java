package com.atlassian.confluence.plugins.socialbookmarking;

import org.apache.commons.lang.StringUtils;

public class BookmarkTitleComparator extends AbstractBookmarkComparator
{
    @Override
    protected int compareInternal(Bookmark leftBookmark, Bookmark rightBookmark)
    {
        return getTitle(leftBookmark).compareTo(getTitle(rightBookmark));
    }

    private String getTitle(Bookmark bookmark)
    {
        return StringUtils.defaultString(bookmark.getTitle());
    }
}
